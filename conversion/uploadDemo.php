<?php

include_once('DPConvertDOCX2.php');
include_once('DPHTMLPublish3.php');

if ($_FILES["file"]["error"] > 0)
{
	echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
}
else
{
	if (file_exists("tempUpload/" . $_FILES["file"]["name"]))
	{
		if(unlink("tempUpload/" . $_FILES["file"]["name"])){
			move_uploaded_file($_FILES["file"]["tmp_name"],
			"tempUpload/" . $_FILES["file"]["name"]);	
			
			$rand = generateRandomString(14);
			mkdir('./tempUpload/'.$rand.'/');
			$inst = new DPConvertDOCX($_FILES["file"]["name"],'./tempUpload/'.$rand.'/','./tempUpload/'.$rand.'/extractedDocx/','./tempUpload/'.$rand.'/extractedHTML/');
			
			$data = $inst->publishDPFormat();
			
			$inst->saveDataStructure();
			
			$inst1 = new DPHTMLPublish($data);

			echo $inst1->publishHTML();
		}
		else{
			echo "Failed Retry";
		}
	}
	else
	{
		$rand = generateRandomString(14);
		mkdir('./tempUpload/'.$rand.'/');
		
		move_uploaded_file($_FILES["file"]["tmp_name"], './tempUpload/'.$rand.'/' . $_FILES["file"]["name"]);
		
		$inst = new DPConvertDOCX($_FILES["file"]["name"],'./tempUpload/'.$rand.'/','./tempUpload/'.$rand.'/extractedDocx/','./tempUpload/'.$rand.'/extractedHTML/');
		
		$data = $inst->publishDPFormat();
		
		$inst->saveDataStructure();
		
		$inst1 = new DPHTMLPublish($data);
		
		// Write the contents back to the file
		file_put_contents('latestfile.php',$rand);
		
		echo $inst1->publishHTML();
		
	}
}

function generateRandomString($length = 14) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
	
?> 