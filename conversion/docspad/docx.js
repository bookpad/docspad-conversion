
// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "docsPadReader",
        defaults = {
            
        };

    // The actual plugin constructor
    function Plugin( element, options ) {
        
        this.element = element;
        
        this.doc = $(element).children('div');
        
        this.docHeight = $(this.doc).height();
        this.docWidth = $(this.doc).width();
        
        this.pageHeight = 0;
        this.currPageNo = 1;
        this.rendered = "<div class = 'DocPage' data-pageNo = '1'><div>";
       
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        
        this.options = $.extend( {}, defaults, options );

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {

        init: function() {
            return this;
        },
                
                
        /*=========================================================================================================================================
         *                  FOR RETURING HEIGHTS AND WIDTHS OF DIFFERENT ELEMENTS
         ===========================================================================================================================================*/
       
                
        paraHeight: function(element){
            return parseFloat(($(element).outerHeight(true) + $(element).outerHeight())/2);
        },
                
        wordHeight: function (element){
            return parseFloat($(element).outerHeight());
        },
                
        pointHeight: function(element){     //point of a list
            return parseFloat($(element).outerHeight());
        },
        
        wordWidth: function(element){
            return parseFloat($(element).outerWidth());
        },
                
        imageHeight: function(element){
            return parseFloat($(element).children('img').attr('height'));
        },
                
        imageWidth: function(element){
            return parseFloat($(element).children('img').attr('width'));
        },
                
        rowHeight: function(element){
            return parseFloat($(element).outerHeight(true));
        },
                
                
        /*=========================================================================================================================================
         *                  FOR RENDERING TABLES
         ===========================================================================================================================================*/
        
        tableToRows: function(element){
            var main = this;
            var rows = {};
           
            $(element).children('tbody').children('tr').each(function(index, row){
                rows[index] = row;
            });
            
            console.log(rows);
            return rows;
        },
        
        exploreTable: function(element){
            var main = this;
            var i = 0;
            var rowHeight;
                        
            var attr = main.outerHTML(element, 0, "<table>");
            attr = attr.substring(0, attr.indexOf('>') + 1);
            var rows =  main.tableToRows(element);
            console.log(rows);
            
            main.rendered = main.rendered + attr;
            
            $.each(rows, function(index, row){
                rowHeight = main.rowHeight(row);
                
                console.log(main.docHeight, main.pageHeight, rowHeight);
                
                if(rowHeight + main.pageHeight > main.docHeight){
                    main.currPageNo++;
                    main.pageHeight = 0;
                    main.rendered = main.rendered + '</table></div></div><div class="DocPage" data-pageNo="' + main.currPageNo + '"><div>' + attr;
                } 
                
                main.rendered = main.rendered + main.outerHTML(row, 0, "<tr>");   
                main.pageHeight = main.pageHeight + rowHeight;
            });
            
            main.rendered = main.rendered + '</table>'; 
        },
         
        
        /*=========================================================================================================================================
         *                  FOR RENDERING LIST
         ===========================================================================================================================================*/
       
        listToPoints: function(element){
            var main = this;
            var points = {};
           
            $(element).children('li').each(function(index, point){
                points[index] = point;
            });
            
            console.log(points);
            return points;
        },        
        
        exploreList: function(element, nested, prevattr, nestdata){         //for lists
            var main = this;
            var i = 0;
            var pointHeight;
            var checkNested;
                        
            var attr = main.outerHTML(element, 0, "<ul>");
            attr = attr.substring(0, attr.indexOf('>') + 1);
            var points =  main.listToPoints(element);
            
            if(nested){
                
                var nestHeight = main.wordHeight($(nestdata));
                if(nestHeight + main.pageHeight > main.docHeight){
                    main.currPageNo++;
                    main.pageHeight = 0;
                    for(i = 0; i < nested; i++){
                        main.rendered = main.rendered + '</ul></li>';
                    }
                    main.rendered = main.rendered + '</ul></div></div><div class = DocPage data-psgeNo ="' + main.currPageNo + '"><div>';
                    for(i = 0; i < nested; i++){
                        main.rendered = main.rendered + prevattr[i].substring(0, prevattr[i].indexOf('>') + 1) + '<li>';
                    }
                    
                    main.rendered = main.rendered + main.outerHTML(nestdata, 0, "<span>");
                    
                } else {
                    main.rendered = main.rendered + '<li>';
                    main.rendered = main.rendered + main.outerHTML(nestdata, 0, "<span>");
                }                                
            }
            
            main.rendered = main.rendered + attr;
            
            $.each(points, function(index, point){
                pointHeight = main.pointHeight(point);
                
                if(pointHeight + main.pageHeight > main.docHeight){
                    checkNested = $(point).children('ul');
                    checkNested = $(checkNested).size();
                    console.log(checkNested);
                    if(checkNested){
                        console.log(point);
                        
                        prevattr[nested] = attr;
                        main.exploreList($(point).children('ul'), nested + 1, prevattr, $(point).children('span'));
                    } else {
                        main.currPageNo++;
                        main.pageHeight = 0;
                        for(i = 0; i < nested; i++){
                            main.rendered = main.rendered + '</ul></li>';
                        }
                        main.rendered = main.rendered + '</ul></div></div><div class = DocPage data-psgeNo ="' + main.currPageNo + '"><div>';
                        for(i = 0; i < nested; i++){
                            main.rendered = main.rendered + prevattr[i].substring(0, prevattr[i].indexOf('>')) + 'style="list-style:none"' + '>' + '<li>';
                        }
                        main.rendered = main.rendered + attr;
                        
                        main.rendered = main.rendered + main.outerHTML(point, 0, "<li>");
                    }
                } else {
                    main.rendered = main.rendered + main.outerHTML(point, 0, "<li>");
                    main.pageHeight = main.pageHeight + pointHeight;
                }
            });
            
            for(i = 0; i < nested; i++){
                main.rendered = main.rendered + '</ul></li>';
            }
            main.rendered = main.rendered + '</ul>';
            
            for(i = 0; i < nested; i++){
                main.rendered =  main.rendered + prevattr[i];
            }
        },
                
        
        /*=========================================================================================================================================
         *                  FOR RENDERING PARAGRAPHS WITH IMAGES AND TEXT
         ===========================================================================================================================================*/
       
        
        paraToLines: function(element){
            var main = this;
            var lines = {};
            var i = 0;
            var lineWidth = 0;
            var wordWidth = 0;
                    
            lines[i] = "<p>";
            
            $(element).children('span').each(function(index, word){
                
                if($(word).attr('data-type')==="image"){
                    wordWidth = main.imageWidth(word);
                    console.log(wordWidth);
                } else {
                    wordWidth = main.wordWidth(word);
                }
                console.log(wordWidth, lineWidth, main.docWidth);
                if(wordWidth + lineWidth > main.docWidth){
                    
                    lineWidth = 0;
                    lines[i] = lines[i] + '</p>';
                    i++;
                    lines[i] = '<p>' + main.outerHTML(word, 0, "<p>");
                } else {
                    lines[i] = lines[i] + main.outerHTML(word, 0, "<p>");
                    lineWidth = lineWidth + wordWidth;
                }
            });
             
            lines[i] = lines[i] + '</p>';
            //console.log(lines);
            return lines;
        },
        
        exploreText: function(element){         //for text and image
            var main = this;
            var i = 0;
            var lineHeight;
            var wordHeight;
            
            var pagefull = false;
            
            var attr = main.outerHTML(element, 0, "<p>");
            attr = attr.substring(0, attr.indexOf('>') + 1);
            var lines =  main.paraToLines(element);
            var remaining = attr;
            
            main.rendered = main.rendered + attr;
            
            //console.log(lines);
            
            $('div.temp').html("");
            
            $.each(lines, function (index, line){
                $('div.temp').append(line);
            });
            
            pagefull = false;
            
            $('div.temp').children('p').each(function(index, line){
                lineHeight = 0;
                
                $(line).children('span').each(function(wordNo, word){
                    if($(word).attr('data-type')==='image'){
                        wordHeight = main.imageHeight(word);
                        //console.log(wordHeight);
                    } else {
                        wordHeight = main.wordHeight(word);
                    }
                    
                    if(wordHeight > lineHeight){
                        lineHeight = wordHeight;
                        //console.log(lineHeight);
                    }
                });
                
                if(main.pageHeight + lineHeight > main.docHeight){
                    pagefull = true;
                    
                    //console.log(line ,main.pageHeight, main.docHeight, lineHeight);
                    //console.log(main.currPageNo);
                    //main.rendered = main.rendered + "dinaldnlaksdn";
                    
                    remaining = remaining + $(line).html();
                } else if(pagefull){
                    remaining = remaining + $(line).html();
                } else {
                    console.log(line);
                    main.rendered = main.rendered + $(line).html();
                    main.pageHeight = main.pageHeight + lineHeight;
                }
            });
            // Check part below this rest working as expected
            remaining = remaining + '</p>';
            
            console.log(remaining);
            
            main.rendered = main.rendered + '</p>';
            
            $('div.temp').html(remaining);
            
            //console.log($('div.temp').children('p'));
            
            if(pagefull){
                main.pageHeight = 0;
                main.currPageNo++;
                main.rendered = main.rendered + '</div></div><div class="DocPage" data-pageNo = "' + main.currPageNo + '"><div>';
                main.exploreText($('div.temp').children('p'));
            }
        },
        
        
        /*=========================================================================================================================================
         *                  RENDER PAGES MASTER FUNCTION
         ===========================================================================================================================================*/
       
        
        renderPages: function(){
            var main = this;
            var paraHeight = 0;
            
            console.log(main.docHeight);

            $(main.doc).children().each(function (index, element){
                paraHeight = main.paraHeight(element);
                
                if($(element).attr('data-image')){
                    main.exploreText(element);
                }
                else if((paraHeight + main.pageHeight) > main.docHeight){
                    //console.log(element);
                    if($(element).attr('data-table')){
                        console.log(element);
                        main.exploreTable(element);
                    } else if($(element).attr('data-list')){
                        main.exploreList(element, 0, {}, null);
                    } else {
                        main.exploreText(element);
                    }
                } else {
                    main.rendered = main.rendered + main.outerHTML(element, 0, "<p>");
                    //console.log(main.outerHTML(element, 0, "<p>"));
                    main.pageHeight = main.pageHeight + paraHeight;
                }
                
                console.log(element, main.pageHeight);
            });
            
            main.rendered = main.rendered + '</div></div>';

            console.log(main.rendered);
            //console.log(main.pageHeight);
            //console.log(main.docHeight);
            
            $("div.rendered").html(main.rendered);
            
            return main.rendered;
        },
                
                
        /*=========================================================================================================================================
         *                  GET OUTER HTML OF DIFFERNET ELEMENTS
         ===========================================================================================================================================*/
       

        outerHTML: function(ele, s, tag) {
            return (s)
            ? ele.before(s).remove()
            : jQuery(tag).append($(ele).eq(0).clone()).html();
        }
        
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );