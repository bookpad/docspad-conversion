<?php

namespace DP;

include_once 'entities/DPDocument.php';

$n = new DPDocument('DOCX');

$n->body->content->addPage();
$pgcounter = $n->body->content->pageCounter;
$n->body->content->pages[$pgcounter - 1]->addNode(TRUE);
$concounter = $n->body->content->pages[$pgcounter - 1]->contentCounter;

$n->body->content->pages[$pgcounter - 1]->elements[$concounter - 1]->content->addRow();

$rowCounter = $n->body->content->pages[$pgcounter - 1]->elements[$concounter - 1]->content->rowCounter;

$n->body->content->pages[$pgcounter - 1]->elements[$concounter - 1]->content->rows[$rowCounter]->addColumnToRow();

echo "<pre>";
print_r($n);
echo "</pre>";

?>