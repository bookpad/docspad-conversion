<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * These are all the file types supported by DocsPad
 *
 * @author niketh
 */
class FileTypes
{
    const PDF = "pdf";
    const DOCX = "docx";
    const PPTX = "pptx";
    const XLSX = "xlsx";
    const EPUB = "EPUB";
}
?>
