<?php

namespace docspad\conversion\classes\base;

use Aws\S3\S3Client;
use Aws\Common\Aws;
use Aws\S3\Model\ClearBucket;

class AWSS3BaseFile {

    // This should have all the necessary functions for our purpose from S3
    // This is because so that even in future if the SDK of amazon changes we need to change only this file and not all the others
    // include properties like proxy also which are readily available in the S3 Class so that they can be tested in every environment.

    protected $client;

    public function __construct($temp) {
        //initialise client
        $this->client = $temp;
    }
    
    protected function getBucketStatus($BucketName){
        // returns 0 if bucket name incorrect
        // returns 1 if bucket exists and belong to us
        // returns 2 if bucket exixts and belongs to someone else
        // returns 3 if bucket does not exist
        
        $temp = $this->client;
        
        if (!$temp->isValidBucketName($BucketName)){
            return 0;
        } else if(!$temp->doesBucketExist($BucketName, true, array())){
            return 3;
        } else {
            $AllBuckets = $this->listAllBuckets();
            $check = 0;

            foreach ($AllBuckets['Buckets'] as $bucket) {
                // Each Bucket value will contain a Name and CreationDate

                if(strcmp($bucket['Name'], $BucketName) == 0 ){
                    $check = 1;
                    break;
                }
            }
            
            if($check == 1){
                return 1;
            } else {
                return 2;
            }
        }
    }

    public function createBucket($BucketName) {
        // create Bucket
        // returns 0 if invalid bucket name
        // returns 2 if bucket already exists and belongs to us
        // returns 3 if bucket exixts and belongs to someone else
        // returns 1 if bucket successfully created
        
        $temp = $this->client;
        $num = $this->getBucketStatus($BucketName);
        
        if($num == 0){
            return 0 ;
        } else if($num == 1 || $num == 2) {
            return ($num+1) ;
        } else {
            $temp->createBucket(array('Bucket' => $BucketName));
            $temp->waitUntilBucketExists(array('Bucket' => $BucketName));
            return 1;
        }
    }

    public function listAllBuckets() {
        //simply sends the array which results from listing all buckets

        $temp = $this->client;
        return $temp->listBuckets();
    }

    public function deleteBucket($BucketName) {
        // delete a Bucket
        // returns 0 if invalid bucket name
        // returns 2 if bucket does not exist
        // returns 3 if bucket exists but does not belong to us
        // returns 1 if bucket successfully deleted

        $temp = $this->client;
        $num = $this->getBucketStatus($BucketName);
        
        if($num == 0){
            return 0;
        } else if($num == 3) {
            return 2;
        } else if($num == 2){
            return 3;
        } else {
            // Delete the objects in the bucket before attempting to delete
            // the bucket
            $clear = new ClearBucket($temp, $BucketName);
            $clear->clear();

            // Delete the bucket
            $temp->deleteBucket(array('Bucket' => $BucketName));

            // Wait until the bucket is not accessible
            $temp->waitUntilBucketNotExists(array('Bucket' => $BucketName));
            return 1;
        }
    }

    public function createObject($BucketName, $ObjectName, $ObjectStream) {
        // create a Object
        // returns 0 if invalid bucket name
        // returns 2 if bucket does not exist
        // returns 3 if bucket exists but does not belong to us
        // returns 4 if object already exists
        // returns model acc to put object if successful
        
        $temp = $this->client;
        $num = $this->getBucketStatus($BucketName);
        
        if($num == 0){
            return 0;
        } else if($num == 3) {
            return 2;
        } else if($num == 2){
            return 3;
        } else {
            
            $list = $this->listAllObjects($BucketName);
            
            $i = 0;
            
            foreach($list as $objects){
                if(strcmp($objects['Key'], $ObjectName) == 0)
                   $i = 1;     
            }
            
            if($i==1){
                return 4;
            } else {
                if($ObjectStream != NULL){
                    $result = $temp->putObject(array(
                        'Bucket' => $BucketName,
                        'Key'    => $ObjectName,
                        'Body'   => fopen($ObjectStream, 'r+')
                    ));
                } else {
                    $result = $temp->putObject(array(
                        'Bucket' => $BucketName,
                        'Key'    => $ObjectName,
                        'Body'   => NULL
                    ));
                }
                /*
                if($ObjectStream != NULL) {
                    $result = $temp->upload($BucketName, $ObjectName,fopen($ObjectStream,'r+'));
                } else {
                    $result = $temp->upload($BucketName, $ObjectName, NULL);
                }
                */
                
                $temp->waitUntilObjectExists(array(
                    'Bucket' => $BucketName,
                    'Key' => $ObjectName,
                ));
                
                return $result;
            }
        }
    }

    public function batchCreateBuckets($ArrayBucketNames) {
        // create Buckets in a batch request
        // automatically handles repeated bucket name in array
        // returns 0 if any invalid bucket name
        // returns 2 if bucket already exists and belongs to us
        // returns 3 if bucket exixts and belongs to someone else
        // returns 1 if bucket successfully created
        
        $i = 1;
        $temp = $this->client;
        
        foreach ($ArrayBucketNames as $BucketName) {
            $num = $this->getBucketStatus($BucketName);

            if($num == 0){
                $i = 0 ;
                break;
            } else if($num == 1 || $num == 2) {
                $i = ($num+1) ;
                break;
            }
        }
        
        if($i == 1){
            foreach ($ArrayBucketNames as $BucketName) {
                $temp->createBucket(array('Bucket' => $BucketName));
                $temp->waitUntilBucketExists(array('Bucket' => $BucketName));
            }
        }
        
        return $i;
    }

    public function batchCreateObjects($BucketName, $ArrayObjectNames) {
        // create Objects in a batch request
    }

    public function batchDeleteBuckets($ArrayBucketNames) {
        // delete Buckets in a batch request
        // handles repeated names in array
        // returns 0 if any invalid bucket name
        // returns 2 if any bucket does not exist
        // returns 3 if any bucket exists but does not belong to us
        // returns 1 if bucket successfully deleted
        
        
        $i = 1;
        $temp = $this->client;
        
        foreach ($ArrayBucketNames as $BucketName) {
            
            $num = $this->getBucketStatus($BucketName);

            if($num == 0){
                $i = 0;
                break;
            } else if($num == 3) {
                $i = 2;
                break;
            } else if($num == 2){
                $i = 3;
                break;
            }
        }
        
        if($i == 1){
            foreach ($ArrayBucketNames as $BucketName) {
                if ($temp->doesBucketExist($BucketName, true, array())){
                    // Delete the objects in the bucket before attempting to delete
                    // the bucket
                    $clear = new ClearBucket($temp, $BucketName);
                    $clear->clear();

                    // Delete the bucket
                    $temp->deleteBucket(array('Bucket' => $BucketName));

                    // Wait until the bucket is not accessible
                    $temp->waitUntilBucketNotExists(array('Bucket' => $BucketName));
                }
            }
        }
        
        return $i;
    }

    public function batchDeleteObjects($BucketName, $ArrayObjectNames) {
        // delete Objects in a batch request
    }

    public function listAllObjects($BucketName) {
        // list all objects of a bucket
        // returns 0 if bucket name invalid
        // returns 2 if bucket does not exist
        // returns 3 if bucket belongs to someone else
        // returns the whole object can use as desired by user of function
        
        $temp = $this->client;
        
        $num = $this->getBucketStatus($BucketName);

        if($num == 0){
            return 0;
        } else if($num == 3) {
            return 2;
        } else if($num == 2){
            return 3;
        } else {
            
            $iterator = $temp->getIterator('ListObjects', array(
                'Bucket' => $BucketName
            ));
            
            return $iterator;
        }
    }

    // Any other functions necessary based on the use in our functions
}

?>
