<?php

namespace docspad\conversion\classes\base;

class AWSRDSBaseFile {

    protected $DBServerName;
    protected $DBUserName;
    protected $DBPassword;
    public $con;

    public function __construct() {
        
    }

    protected function escapeString($string) {
        return mysqli_real_escape_string($this->con, $string);
    }

    public function DBConnect($host, $user, $password, $port) {
// connect to DBServer
// returns 1 if successful else the corressponding error

        $this->con = mysqli_connect($host, $user, $password, null, $port);

        if (mysqli_connect_errno()) {
            return mysqli_connect_error();
        } else {
            return 1;
        }
    }

    public function DBSelect($DBName) {
// Select DB
// returns 1 if successful else error number

        $check = mysqli_select_db($this->con, $DBName);

        if ($check) {
            return 1;
        } else {
            return mysqli_errno($this->con);
        }
    }

    public function getNumberOfRows($resp) {
//returns number of rows returned in response of a query

        return mysqli_num_rows($resp);
    }

    public function getAssocArray($resp) {
//returns associative array of rows returned in response of a query

        return mysqli_fetch_assoc($resp);
    }

    public function searchQueryAnd($TableName,  $ColumnsToGet, $ColumnValueArray = NULL) {
//Column Value array is an array of array each conationing 3 attributes/key values
//'name' for column name
//'operator' for '=', '<', '<=', etc whatever is desired by user
//'value' for desired column value
//returns response from query if successful else corresponding error number
//retuens 0 if recognized problem in input
// ColumnsToGet is array of all columns to be retrieved
// Null valued $ColumnsToget retrieves all data 

        $TableName = $this->escapeString($TableName);
        $query = "select ";

        $i = 0;

        if ($ColumnsToGet == NULL) {
            $query = $query . "*";
        } else {
            foreach ($ColumnsToGet as $value) {
                $value = $this->escapeString($value);

                if ($i > 0)
                    $query = $query . ",";

                $query = $query . $value;

                $i++;
            }
        }

        $query = $query . " from " . $TableName . "";

        $i = 0;

        if ($ColumnValueArray == NULL) {
            
        } else {
            $query = $query . " where";

            foreach ($ColumnValueArray as $value) {

                if ($i > 0)
                    $query = $query . ' and ';

                if (!(isset($value['name']) && isset($value['operator']) && isset($value['value']))) {
                    return 0;
                } else {
                    $value['name'] = $this->escapeString($value['name']);
                    $value['operator'] = $this->escapeString($value['operator']);
                    $value['value'] = $this->escapeString($value['value']);

                    $query = $query . " " . $value['name'] . $value['operator'] . "'" . $value['value'] . "'";

                    $i++;
                }
            }
        }

        if (mysqli_errno($this->con)) {
            return mysqli_errno($this->con);
        }

        return mysqli_query($this->con, $query);
    }

    public function insertQuery($Tablename, $ColumnValuesArray) {
//Column Values Array is array with column names as key and corresponding values as values
//returns 1 if successful else corresponding error number

        $Tablename = $this->escapeString($Tablename);

        $temp = "insert into " . $Tablename . " (";
        $query = "";

        foreach ($ColumnValuesArray as $key => $value) {
            $key = $this->escapeString($key);
            $value = $this->escapeString($value);

            $query = $temp . $key;
            $temp = $query . ",";
        }

        $temp = $query . ") values (";

        foreach ($ColumnValuesArray as $key => $value) {
            $query = $temp . "'" . $value . "'";
            $temp = $query . ",";
        }

        $query = $query . ")";

        $resp = mysqli_query($this->con, $query);

        if ($resp) {
            return 1;
        } else {
            return mysqli_errno($this->con);
        }
    }

    public function deleteQueryAnd($TableName, $ColumnValueArray) {
//Column Values Array is array with column names as key and corresponding values as values
//returns 1 if successful else corresponding error number
//Column Value array is an array of array each conationing 3 attributes/key values
//'name' for column name
//'operator' for '=', '<', '<=', etc whatever is desired by user
//'value' for desired column value
//returns response from query if successful else corresponding error number
//retuens 0 if recognized problem in input

        $TableName = $this->escapeString($TableName);

        $query = "delete from " . $TableName . " where";

        $i = 0;

        foreach ($ColumnValueArray as $value) {

            if ($i > 0)
                $query = $query . ' and ';

            if (!(isset($value['name']) && isset($value['operator']) && isset($value['value']))) {
                return 0;
            } else {
                $value['name'] = $this->escapeString($value['name']);
                $value['operator'] = $this->escapeString($value['operator']);
                $value['value'] = $this->escapeString($value['value']);

                $query = $query . " " . $value['name'] . $value['operator'] . "'" . $value['value'] . "'";

                $i++;
            }
        }
        $resp = mysqli_query($this->con, $query);
        
        if ($resp) {
            return 1;
        } else {
            return mysqli_errno($this->con);
        }
    }

    public function updateQuery($Tablename, $ColumnsToUpdateArray, $IdentifyingColumn, $IdentifyingValue) {
//ColumnsToUpdateArray is array with key as column name and value as new value
//IdentifyingColumn is the column which uniquely finds a row and IdentifyingValue is the Corresponding Value
//returns 1 if successful else corresponding mysqli error number

        $Tablename = $this->escapeString($Tablename);
        $IdentifyingColumn = $this->escapeString($IdentifyingColumn);
        $IdentifyingValue = $this->escapeString($IdentifyingValue);

        $temp = "update " . $Tablename . " set ";
        $query = "";

        foreach ($ColumnsToUpdateArray as $key => $value) {
            $key = $this->escapeString($key);
            $value = $this->escapeString($value);
            $query = $temp . $key . "='" . $value . "'";
            $temp = $query . ",";
        }

        $query = $query . " where " . $IdentifyingColumn . "='" . $IdentifyingValue . "'";

        $resp = mysqli_query($this->con, $query);

        echo "<br>" . $query . "<br>";

        if ($resp) {
            return 1;
        } else {
            return mysqli_errno($this->con);
        }
    }

    public function createTable($TableName, $ColumnName, $ColumnType, $ColumnSize = NULL, $DefaultValue = NULL, $AdditionalOptions = NULL) {
// Create Table
// // Additional Options include not null, unique, and all other details
// Additional Options should necessarily be an array with all required properties as values(NOT keys), and key does not matter(favourably numbers)
// returns 1 if successful else error number

        $TableName = $this->escapeString($TableName);
        $ColumnName = $this->escapeString($ColumnName);
        $ColumnType = $this->escapeString($ColumnType);

        $sql = "CREATE TABLE " . $TableName . " (" . $ColumnName . " " . $ColumnType;

        if ($ColumnSize != NULL) {
            $ColumnSize = $this->escapeString($ColumnSize);
            $sql = $sql . "(" . $ColumnSize . ")";
        }

        if ($AdditionalOptions != NULL) {
            foreach ($AdditionalOptions as $value) {
                $value = $this->escapeString($value);
                $sql = $sql . " " . $value;
            }
        }

        if ($DefaultValue != NULL) {
            $DefaultValue = $this->escapeString($DefaultValue);
            $sql = $sql . " DEFAULT '" . $DefaultValue . "'";
        }

        $sql = $sql . ")";

        if (mysqli_query($this->con, $sql)) {
            return 1;
        } else {
            return mysqli_errno($this->con);
        }
    }

    public function createDB($DBName) {
// Create DB
// returns 1 if successful else corresponding error number

        $DBName = $this->escapeString($DBName);

        print_r($DBName);

        $sql = "CREATE DATABASE " . $DBName . "";
        if (mysqli_query($this->con, $sql)) {
            return 1;
        } else {
            return mysqli_errno($this->con);
        }
    }

    public function addColumn($TableName, $ColumnName, $ColumnType, $ColumnSize = NULL, $DefaultValue = NULL, $AdditionalOptions = NULL) {
// Additional Options include not null, unique, and all other details
// Additional Options should necessarily be an array with all required properties as values(NOT keys), and key does not matter
// returns 1 if successful else error number

        $TableName = $this->escapeString($TableName);
        $ColumnName = $this->escapeString($ColumnName);
        $ColumnType = $this->escapeString($ColumnType);

        $sql = "ALTER TABLE " . $TableName . " ADD " . $ColumnName . " " . $ColumnType;

        if ($ColumnSize != NULL) {
            $ColumnSize = $this->escapeString($ColumnSize);
            $sql = $sql . "(" . $ColumnSize . ")";
        }

        if ($AdditionalOptions != NULL) {
            foreach ($AdditionalOptions as $value) {
                $value = $this->escapeString($value);
                $sql = $sql . " " . $value;
            }
        }

        if ($DefaultValue != NULL) {
            $DefaultValue = $this->escapeString($DefaultValue);
            $sql = $sql . " DEFAULT '" . $DefaultValue . "'";
        }

        if (mysqli_query($this->con, $sql)) {
            return 1;
        } else {
            return mysqli_errno($this->con);
        }
    }

// delete, deleteBatch, insertvalues, insertvaluesbatch,
}

?>
