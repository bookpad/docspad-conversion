<?php

namespace docspad\conversion\classes\manager;

require_once __DIR__.'/../base/AWSRDSBaseFile.php';
require_once __DIR__.'/../base/GeneralBaseFile.php';
require_once __DIR__.'/../base/AWSLogin.php';

class DPCompany {

    protected $companyUsername;
    protected $companyAccessKeyID;
    protected $companyAccessKeyIDSecretKey;
    protected $companyEmailID;
    protected $companyUniqueID;
    protected $companyLoginStatus;
    protected $RDSBaseClassObject;
    protected $GeneralBaseClassObject;
    protected $S3BaseClassObject;

    public function __construct() {
        $this->companyAccessKeyID = NULL;
        $this->companyAccessKeyIDSecretKey = NULL;
        $this->companyEmailID = NULL;
        $this->companyLoginStatus = FALSE;
        $this->companyUniqueID = NULL;
        $this->companyUsername = NULL;
        $this->RDSBaseClassObject = new \docspad\conversion\classes\base\AWSRDSBaseFile();
        $this->GeneralBaseClassObject = new \docspad\conversion\classes\base\GeneralBaseFile();
//        $temp = new \docspad\conversion\classes\base\AWSLogin('S3');
//        $this->S3BaseClassObject = new AWSS3BaseFile($temp->client);

        $this->RDSBaseClassObject->DBConnect('dpc.cyz5tjcfwgjz.us-east-1.rds.amazonaws.com:3306', 'test123qwerty', 'ytrewq321tset');
        $this->RDSBaseClassObject->DBSelect('DB_DocsPad');
    }

    public function login($username, $password) {
        // Verify Company Credentials if fails call the company verification status and throw exception that company not verified. 
        // returns 1 if successful else 0
        $ob = $this->RDSBaseClassObject;
        $resp = $ob->searchQueryAnd('DB_company', array('DPC_AccessKeyID', 'DPC_AccessKeyIDSecretKey', 'DPC_EmailID', 'DPC_UniqueId') ,array(array('name' => 'DPC_Username', 'operator' => '=', 'value' => $username), array('name' => 'DPC_Password', 'operator' => '=', 'value' => $password)));

        if(!is_nan($resp)) return 0;
        
        if ($ob->getNumberOfRows($resp) > 0) {
            $array = $ob->getAssocArray($resp);
            $this->companyUsername = $username;
            $this->companyAccessKeyID = $array['DPC_AccessKeyID'];
            $this->companyAccessKeyIDSecretKey = $array['DPC_AccessKeyIDSecretKey'];
            $this->companyEmailID = $array['DPC_EmailID'];
            $this->companyUniqueID = $array['DPC_UniqueID'];
            $this->companyLoginStatus = TRUE;

            return 1;
        } else {
            return 0;
        }
    }

    public function logout() {
        $this->companyAccessKeyID = NULL;
        $this->companyAccessKeyIDSecretKey = NULL;
        $this->companyEmailID = NULL;
        $this->companyLoginStatus = FALSE;
        $this->companyUniqueID = NULL;
        $this->companyUsername = NULL;
    }

    public function getLoginStatus() {
        //returns 1 if logged in 0 if logged out
        if ($this->companyLoginStatus)
            return 1;
        else
            return 0;
    }

    public function changePassword($oldPassword, $newPassword, $confirmPassword) {
        // Change Password of the Company
        // returns 1 if successful
        // returns 3 if new password mistyped i.e. newPassword and confirmPassword do not match
        // returns 2 if old password does not match
        // returns -1 if not logged in
        // returns corresponding error number if data correct but process unsuccessful

        if (!$this->companyLoginStatus){
            return -1;
        }
        
        if (strcmp($newPassword, $oldPassword) != 0) {
            return 3;
        }

        $ob = $this->RDSBaseClassObject;
        
        $resp = $ob->searchQueryAnd('DB_company' ,array('DPC_Username'), array(array('name' => 'DPC_Username', 'operator' => '=', 'value' => $this->companyUsername), array('name' => 'DPC_Password', 'operator' => '=', 'value' => $oldPassword)));
        
        if(!is_nan($resp)) return $resp;
        
        if($ob->getNumberOfRows($resp)>0){
            return $ob->updateQuery('DB_company', array('DPC_Password' => $newPassword), 'DPC_UniqueID', $this->companyUniqueID);
        } else {
            return 2;
        }
    }

    public function getCompanyInfo() {
        // Get user level, screen name, address, lastvisitedip, lastvisitedtime, and all other available details.
        // returns -1 if not logged in
        // returns 2 for unknown system error
        // returns array of key value pairs if successful else corresponding error number
        
        if(!$this->companyLoginStatus){
            return -1;
        }
        
        $ob = $this->RDSBaseClassObject;
        
        $resp = $ob->searchQueryAnd('DB_company',array('DPC_AWSS3Expenses','DPC_AWSEC2Expenses','DPC_AWSSQSExpenses','DPC_AWSSESExpenses','DPC_UserLevel','DPC_ScreenName','DPC_DateCreated','DPC_Address','DPC_dateUpdated','DPC_UpdateInfo','DPC_LastVisited','DPC_LastVisitedIP','DPC_CompanyVerify') ,array(array('name' => 'DPC_UniqueID', 'operator' => '=', 'value' => $this->companyUniqueID)));
        
        if(!is_nan($resp)) return $resp;
        
        if($ob->getNumberOfRows($resp)>0){
            $array = $ob->getAssocArray($resp);
            $expenses = $array['DPC_AWSS3Expenses'] + $array['DPC_AWSEC2Expenses'] + $array['DPC_AWSSQSExpenses'] + $array['DPC_AWSSESExpenses'];
           //imp edit below part as required genearlise and structure if required
             return array('User_Level' => $array['DPC_UserLevel'], 'Screen_Name' => $array['DPC_ScreenName'], 'Address' => $array['DPC_Address'], 'Last_Visited_IP' => $array['DPC_LastVisitedIP'], 'Last_Visited_Time' => $array['DPC_LastVisited'], 'Date_Created'=>$array['DPC_DateCreated'], 'Date_Updated' => $array['DPC_DateUpdated'], 'Update_Info' => $array['DPC_UpdateInfo'], 'Company_Verification'=>$array['DPC_CompanyVerify'], 'Company_Expenses' => $expenses);
        } else {
            return 2;
        }
        
    }

    public function getCompanyAWSExpenses() {
        // Get the user expenses from all expenses columns.
        // returns -1 if not logged in
        // returns -2 if unknown system error
        // returns total expenses if successful
        
        if(!$this->companyLoginStatus){
            return -1;
        }
        
        $ob= $this->RDSBaseClassObject;
        
        $resp = $ob->searchQueryAnd('DB_company', array('DPC_AWSS3Expenses','DPC_AWSSESExpenses','DPC_AWSEC2Expenses','DPC_AWSSQSExpenses'),array(array('name' => 'DPC_UniqueID', 'operator' => '=', 'value' => $this->companyUniqueID)));
        
        if(!is_nan($resp)) return -2;
        
        if($ob->getNumberOfRows($resp)>0){
            $array = $ob->getAssocArray($resp);
            
            $expenses = $array['DPC_AWSS3Expenses'] + $array['DPC_AWSEC2Expenses'] + $array['DPC_AWSSQSExpenses'] + $array['DPC_AWSSESExpenses'];
            
            return $expenses;
            
        } else {
            return -2;
        }
    }

    public function getCompanyVerficationStatus() {
        // Get companies verification Status
        // returns -1 if not logged in
        // returns -2 if unknown error
        
        if(!$this->companyLoginStatus){
            return -1;
        }
        
        $ob= $this->RDSBaseClassObject;
        
        $resp = $ob->searchQueryAnd('DB_company', array('DPC_CompanyVerify'),array(array('name' => 'DPC_UniqueID', 'operator' => '=', 'value' => $this->companyUniqueID)));
        
        if(!is_nan($resp)) return -2;
        
        if($ob->getNumberOfRows($resp)>0){
            $array = $ob->getAssocArray($resp);
            
            return $array['DPC_CompanyVerify'];
            
        } else {
            return -2;
        }
    }

    public function verifyCompany($username, $password) {
        // verify the company credentials
    }

    public function changeAddress($newAddress) {
        // Change Address of the company
        // returns 1 if successful else error number
        
        $ob = $this->RDSBaseClassObject;
        
        if(!$this->companyLoginStatus){
            return -1;
        }
        
        $resp = $ob->updateQuery('DB_company', array('DPC_Address' => $newAddress), 'DPC_UniqueID', $this->companyUniqueID);
        
        return $resp;
    }
    
    public function listUsers($fieldsToRetrieve = NULL){
        // fieldsToRetrive is an array of required fields as values
        // returns array of all user details
        // null value gets all data
        // returns -2 if unknown error
        // returns -3 if no users
        
        $ob = $this->RDSBaseClassObject;
        
        if(!$this->companyLoginStatus){
            return -1;
        }
        
        $resp = $ob->searchQueryAnd('DB_companyuser', $fieldsToRetrieve, $ColumnValueArray = array(array('name' => 'DPCU_UniqueId', 'operator' => '=', 'value' => $this->companyUniqueID)));
        
        if(!is_nan($resp)) return -2;
        
        if($ob->getNumberOfRows($resp)>0){
            
        } else {
            return -3;
        }
    }
    
    
    public function createUser($email, $password, $screenName){
        
        // creates a new user of a company
        // returns 1 if successful
        // returns 0 if upload error
        // returns 2 if user already exists
        // returns -1 if company not logged in
        // returns 4 if unknown major error delete user again
        
        $ob = $this->RDSBaseClassObject;
        $ob2 = $this->GeneralBaseClassObject;
        $ob3 = $this->S3BaseClassObject;
        
        $key = $ob2->Generate32BitAlphaNumericKey();
        
        if(!$this->companyLoginStatus)  return -1;
        $user = $this->companyUniqueID . '#' . $email;
        
        $resp = $ob->insertQuery('DB_companyuser', array('DPCU_UserID' => $key, 'DPCU_EmailAddress' => $user, 'DPCU_Password' => $password, 'DPCU_ScreenName' => $screenName, 'DPCU_UniqueID' => $this->companyUniqueID));
    
        $directory = 'BC_' . $this->companyUniqueID . '/BCU_' . $key;
        
        if($resp){
            
            $result = $ob3->createObject('',$directory , NULL);
            
            if(!is_nan($result)){
                $resp2 = $ob->deleteQueryAnd('DB_companyuser', array(array('name' => 'DPCU_UserID', 'operator' => '=', 'value' => $key)));
                
                if($resp2){
                    return 0;
                } else {
                    return 4;
                }         
            }
            
            return 1;
        } else {
            $resp = $ob->searchQueryAnd('DB_companyuser', array('DPCU_UserID') ,array(array('name' => 'DPCU_EmailAddress','operator'=>'=','value' => $user)));
            if($ob->getNumberOfRows($resp)>0){
                return 2;
            } else {
                do{
                    $key = $ob2->Generate32BitAlphaNumericKey();
                    $resp = $ob->insertQuery('DB_companyuser', array('DPCU_UserID' => $key, 'DPCU_EmailAddress' => $user, 'DPCU_Password' => $password, 'DPCU_ScreenName' => $screenName, 'DPCU_UniqueID' => $this->companyUniqueID));
                }while(!$resp);
                
                $result = $ob3->createObject('',$directory , NULL);

                if(!is_nan($result)){
                    $resp2 = $ob->deleteQueryAnd('DB_companyuser', array(array('name' => 'DPCU_UserID', 'operator' => '=', 'value' => $key)));

                    if($resp2){
                        return 0;
                    } else {
                        return 4;
                    }         
                }
                return 1;
            }
        }
    }

    
}

?>
