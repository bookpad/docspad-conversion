<?php

class DPCompanyUser {

    protected $UserID;
    protected $CompanyUniqueID;
    protected $UserScreenName;
    protected $UserEmail;
    protected $UserLoginStatus;
    protected $RDSBaseClassObject;
    protected $S3BaseClassObject;

    public function __construct() {
        $this->UserLoginStatus = FALSE;
        $this->UserEmail = NULL;
        $this->UserID = NULL;
        $this->CompanyUniqueID = NULL;

        $this->RDSBaseClassObject = new AWSRDSBaseFile;
        $this->S3BaseClassObject = new AWSS3BaseFile;
    }

    public function login($email, $password, $companyId) {
        // returns 1 for success else 0
        
        $ob = $this->RDSBaseClassObject;

        $user = $companyId . "#" . $email;
        
        $resp = $ob->searchQueryAnd('DB_companyuser', array('DPCU_UniqueID', 'DPCU_EmailAddress', 'DPCU_ScreenName') ,array(array('name' => 'DPCU_EmailAddress', 'operator' => '=', 'value' => $user), array('name' => 'DPCU_Password', 'operator' => '=', 'value' => $password)), array('DPCU_UserID', 'DPCU_EmailAddress', 'DPCU_ScreenName'));
        
        if (!is_nan($resp))
            return 2;

        if ($ob->getNumberOfRows($resp) > 0) {
            $array = $ob->getAssocArray($resp);

            $this->CompanyUniqueID = $array['DPCU_UniqueID'];
            $this->UserEmail = $array['DPCU_EmailAddress'];
            $this->UserLoginStatus = TRUE;
            $this->UserID = $email;
            $this->UserScreenName = $array['DPCU_ScreenName'];

            return 1;
        } else {
            return 0;
        }
    }
    
    public function upload($bucket, $fileName, $fileStream = NULL){
        // returns -1 if logged out
        // returns 0 if invalid bucket name
        // returns 2 if bucket does not exist
        // returns 3 if bucket exists but does not belong to us
        // returns 4 if object already exists
        // returns model acc to put object if successful
        
        $ob = $this->S3BaseClassObject;
        
        if(!$this->UserLoginStatus) return -1;
        
        $resp = $ob->createObject($bucket, $fileName, $fileStream);
        
        return $resp;
    }

}

?>