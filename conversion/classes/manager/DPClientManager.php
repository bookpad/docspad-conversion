<?php
namespace docspad\conversion\classes\manager;

require_once __DIR__.'/../exceptions/InvalidAPIKeyException.php';
require_once __DIR__.'/../exceptions/ExpiredAPIKeyException.php';
require_once __DIR__.'/../base/AWSRDSBaseFile.php';
require_once __DIR__.'/../base/GeneralBaseFile.php';
require_once __DIR__.'/../base/AWSLogin.php';

/**
 * Description of DPCLIENTS
 *
 * @author niketh
 */
class DPClientManager {
    protected $clientId;
    protected $apiKeys;
    
    protected $RDSBaseClassObject;
    protected $GeneralBaseClassObject;
    
    public function __construct() {
        $this->RDSBaseClassObject = new \docspad\conversion\classes\base\AWSRDSBaseFile();
        $this->GeneralBaseClassObject = new \docspad\conversion\classes\base\GeneralBaseFile();
        
        $this->RDSBaseClassObject->DBConnect('5.79.36.109', 'test123qwerty', 'ytrewq321tset', '3306');
        $this->RDSBaseClassObject->DBSelect('DB_DocsPad');
    }
    
    public function getClientId($apikey){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                
        if (!($stmt = $conn->prepare("SELECT `DP_CLIENTS`.`DP_CLIENT_ID`,  `DP_CLIENTS`.`DP_IS_KEY_ACTIVE`".
                " FROM `DB_DocsPad`.`DP_CLIENTS` WHERE `DP_CLIENTS`.`DP_API_KEY` = ?"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("s", $apikey)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!($result = $stmt->execute())) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        /* bind variables to prepared statement */
        $stmt->bind_result($clientIdCol, $active);
        
        $stmt->store_result();
        $nrows = $stmt->num_rows;
        
        if($nrows == 0 || $nrows > 1){
            throw new \InvalidAPIKeyException("Invalid api key has been supplied", "302");
        }
        
        $stmt->fetch();
         
        if($active == '0'){
            throw new \ExpiredAPIKeyException("API key being supplied has expired", "302");
        }
        
        return $clientIdCol;     
    }
    
    public function createClient() {
    }
 
    public function getAllClients() {
    }
    
    public function setAPIKey(){
        
    }
    
    public function getAllAPIKeys(){
        
    }    
}

?>
