<?php
namespace docspad\conversion\classes\manager;

require_once __DIR__.'/../base/AWSRDSBaseFile.php';
require_once __DIR__.'/../base/GeneralBaseFile.php';
require_once __DIR__.'/../base/AWSLogin.php';
require_once __DIR__.'/../exceptions/InvalidDocIdException.php';
require_once __DIR__.'/../exceptions/DocDeletedException.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DPAPILogManager
 *
 * @author niketh
 */
class DPAPILogManager {
    protected $docId;
    protected $clientId;
    
    protected $RDSBaseClassObject;
    protected $GeneralBaseClassObject;
    
    public function __construct() {
        $this->RDSBaseClassObject = new \docspad\conversion\classes\base\AWSRDSBaseFile();
        $this->GeneralBaseClassObject = new \docspad\conversion\classes\base\GeneralBaseFile();
        
        $this->RDSBaseClassObject->DBConnect('5.79.36.109', 'test123qwerty', 'ytrewq321tset', '3306');
        $this->RDSBaseClassObject->DBSelect('DB_DocsPad');
    }
    
    public function insertDownloadLog($clientId, $docId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
        
        $date = date_create();
        $date_str = date_format($date, 'Y-m-d H:i:s') . "\n";
        
        $requestType = "DOWNLOAD";
                
        if (!($stmt = $conn->prepare("INSERT INTO `DB_DocsPad`.`DP_API_LOG` ".
                "(`DP_CLIENT_ID`, `DP_API_TYPE`, `DP_REQUEST_DATE`, `DP_DOC_ID`) ".
                " VALUES (?, ?, ?, ?)"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ssss", $clientId, $requestType, $date_str, 
                $docId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
    }
}

?>
