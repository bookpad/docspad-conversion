<?php
namespace docspad\conversion\classes\manager;

require_once __DIR__.'/../base/AWSRDSBaseFile.php';
require_once __DIR__.'/../base/GeneralBaseFile.php';
require_once __DIR__.'/../base/AWSLogin.php';
require_once __DIR__.'/../exceptions/InvalidDocIdException.php';
require_once __DIR__.'/../exceptions/DocDeletedException.php';
require_once __DIR__.'/../exceptions/NoteDeletedException.php';
require_once __DIR__.'/../exceptions/InvalidNoteIdException.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DPDOCUMENTS
 *
 * @author niketh
 */
class DPNotesManager{
    protected $docId;
    protected $clientId;
    
    protected $RDSBaseClassObject;
    protected $GeneralBaseClassObject;
    
    public function __construct() {
        $this->RDSBaseClassObject = new \docspad\conversion\classes\base\AWSRDSBaseFile();
        $this->GeneralBaseClassObject = new \docspad\conversion\classes\base\GeneralBaseFile();
        
        $this->RDSBaseClassObject->DBConnect('5.79.36.109', 'test123qwerty', 'ytrewq321tset', '3306');
        $this->RDSBaseClassObject->DBSelect('DB_DocsPad');
    }


    public function insertNote($clientId, $docId, $noteId, $page, $content, $posX, $posY) {
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
        
        $date = date_create();
        $date_str = date_format($date, 'Y-m-d H:i:s') . "\n";
        
        $status = "CONVERTING";
        $active = 1;
                
        if (!($stmt = $conn->prepare("INSERT INTO `DB_DocsPad`.`DP_NOTES` (`DP_CLIENT_ID`, ".
                " `DP_DOC_ID`, `DP_NOTE_ID`, `DP_PAGE_INFO`, `DP_CONTENTS`, `DP_CREATED_DATE`, ".
                " `DP_UPDATED_DATE`, `DP_IS_ACTIVE`, `DP_POSX`, `DP_POSY`) VALUES (?, ?, ?, ?, ?, ?, ?, 1, ?, ?) "))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("sssssssss", $clientId, $docId, $noteId, $page, $content, $date_str, $date_str, $posX, $posY)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
    }
    
    public function getNote($clientId, $noteId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("SELECT DP_CONTENTS, DP_IS_ACTIVE, DP_PAGE_INFO, DP_POSX, DP_POSY ".
                " FROM DB_DocsPad.DP_NOTES ".
                " WHERE DP_CLIENT_ID = ? AND DP_NOTE_ID = ? AND DP_IS_ACTIVE = 1"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ss", $clientId, $noteId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt->bind_result($contents, $active, $pageNo, $posX, $posY);
        $stmt->store_result();
        $nrows = $stmt->num_rows;
        
        if($nrows == 0){
            throw new \InvalidNoteIdException("Invalid Note Id", "302");
        }
        
        //Fetch results for query
        $stmt->fetch();

        if($active == 0){
            throw new \NoteDeletedException("Invalid Doc Id", "302");
        }
        
        return array("contents" => $contents, "page" => $pageNo, "posX" => $posX, "posY" => $posY);
    }
        
     public function getAllNotes($clientId, $docId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("SELECT DP_NOTE_ID, DP_CONTENTS, DP_IS_ACTIVE, DP_PAGE_INFO, DP_POSX, DP_POSY ".
                " FROM DB_DocsPad.DP_NOTES ".
                " WHERE DP_CLIENT_ID = ? AND DP_DOC_ID = ? AND DP_IS_ACTIVE = 1"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ss", $clientId, $docId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt->bind_result($note, $contents, $active, $pageNo, $posX, $posY);
        $stmt->store_result();
        $nrows = $stmt->num_rows;
        
        if($nrows == 0){
            throw new \InvalidNoteIdException("Invalid Note Id", "302");
        }
        
        $notes = array();
        while($stmt->fetch()){
            //Fetch results for query
            array_push($notes, array("contents" => $contents, "page" => $pageNo, 
                "noteId" => $note, "posX" => $posX, "posY" => $posY));
        
        }
        
        return $notes;
    }
    
    
     public function editNote($clientId, $noteId, $contents){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("UPDATE DB_DocsPad.DP_NOTES ".
                " SET DP_CONTENTS = ? ".
                " WHERE DP_CLIENT_ID = ? AND DP_NOTE_ID = ? "))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("sss", $contents, $clientId, $noteId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
//            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function deleteNote($clientId, $noteId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("UPDATE `DB_DocsPad`.`DP_NOTES` ".
                " SET DP_IS_ACTIVE = '0' ".
                " WHERE DP_CLIENT_ID = ? AND ".
                "       DP_NOTE_ID = ?"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ss", $clientId, $noteId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt->store_result();
        $nrows = $stmt->affected_rows;
        
        if($nrows == 0){
            throw new \InvalidNoteIdException("Invalid Note Id", "302");
        }
        
        return true;
    }
    

}

?>
