<?php
namespace docspad\conversion\classes\manager;

require_once __DIR__.'/../../../conversion/aws/aws-autoloader.php';
use Aws\Common\Aws;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of S3Manager
 *
 * @author niketh
 */
class S3Manager {
    public $aws;
    public function __construct(){
        $this->aws = Aws::factory(array(
            'key'    => 'AKIAISFAOASAECDWEO3A',
            'secret' => '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'
//            'region' => Region::US_WEST_2
        ));
    }
    
    public function uploadFile($bucket, $key, $filepath){
        $s3 = $this->aws->get('s3');
        // Upload an object by streaming the contents of a file
        // $pathToFile should be absolute path to a file on disk
        $result = $s3->putObject(array(
            'Bucket'     => $bucket,
            'Key'        => $key,
            'Body' => $filepath
        ));
    }
    
    public function uploadFileWithPath($bucket, $key, $filepath){
        $s3 = $this->aws->get('s3');
        // Upload an object by streaming the contents of a file
        // $pathToFile should be absolute path to a file on disk
        $result = $s3->putObject(array(
            'Bucket'     => $bucket,
            'Key'        => $key,
            'SourceFile' => $filepath
        ));
    }
    
    /**
    * Streams an object from Amazon S3 to the browser
    *
    * @param S3Client $client Client used to send requests
    * @param string   $bucket Bucket to access
    * @param string   $key    Object to stream
    */
    function readObject($bucket, $key){
        // Register the Amazon S3 stream wrapper
        $client = $this->aws->get('s3');
        $client->registerStreamWrapper();
        
        // Begin building the options for the HeadObject request
        $options = array('Bucket' => $bucket, 'Key' => $key);

        // Check if the client sent the If-None-Match header
        if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
            $options['IfNoneMatch'] = $_SERVER['HTTP_IF_NONE_MATCH'];
        }

        // Check if the client sent the If-Modified-Since header
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
            $options['IfModifiedSince'] = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
        }

        // Create the HeadObject command
        $command = $client->getCommand('HeadObject', $options);

        try {
            $response = $command->getResponse();
        } catch (Aws\S3\Exception\S3Exception $e) {
            // Handle 404 responses
            http_response_code(404);
            exit;
        }

        // Set the appropriate status code for the response (e.g., 200, 304)
        $statusCode = $response->getStatusCode();
//        http_response_code($statusCode);
        header('X-PHP-Response-Code: '.$statusCode, true, $statusCode);

        // Let's carry some headers from the Amazon S3 object over to the web server
        $headers = $response->getHeaders();
        $proxyHeaders = array(
            'Last-Modified',
            'ETag',
            'Content-Type',
            'Content-Disposition'
        );

        foreach ($proxyHeaders as $header) {
            if ($headers[$header]) {
                header("{$header}: {$headers[$header]}");
            }
        }

        // Stop output buffering
        if (ob_get_level()) {
            ob_end_flush();
        }

        flush();

        // Only send the body if the file was not modified
        if ($statusCode == 200) {
            readfile("s3://{$bucket}/{$key}");
        }
    }

}

?>
