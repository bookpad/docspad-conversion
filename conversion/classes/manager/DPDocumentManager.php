<?php
namespace docspad\conversion\classes\manager;

require_once __DIR__.'/../base/AWSRDSBaseFile.php';
require_once __DIR__.'/../base/GeneralBaseFile.php';
require_once __DIR__.'/../base/AWSLogin.php';
require_once __DIR__.'/../exceptions/InvalidDocIdException.php';
require_once __DIR__.'/../exceptions/DocDeletedException.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DPDOCUMENTS
 *
 * @author niketh
 */
class DPDocumentManager{
    protected $docId;
    protected $clientId;
    
    protected $RDSBaseClassObject;
    protected $GeneralBaseClassObject;
    
    public function __construct() {
        $this->RDSBaseClassObject = new \docspad\conversion\classes\base\AWSRDSBaseFile();
        $this->GeneralBaseClassObject = new \docspad\conversion\classes\base\GeneralBaseFile();
        
        $this->RDSBaseClassObject->DBConnect('5.79.36.109', 'test123qwerty', 'ytrewq321tset', '3306');
        $this->RDSBaseClassObject->DBSelect('DB_DocsPad');
    }


    public function insertDocument($clientId, $docId, $oriFilename, $remoteFilename, $filesize) {
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
        
        $date = date_create();
        $date_str = date_format($date, 'Y-m-d H:i:s') . "\n";
        
        $status = "CONVERTING";
        $active = 1;
                
        if (!($stmt = $conn->prepare("INSERT INTO `DB_DocsPad`.`DP_DOCUMENTS` (`DP_DOC_ID`, `DP_CLIENT_ID`,`DP_ORI_FILENAME`, ".
                "`DP_DOCUMENTS`.`DP_CREATED_DATE`, `DP_DOCUMENTS`.`DP_UPDATED_DATE`, `DP_DOCUMENTS`.`DP_CONVERSION_STATUS`, ".
                "`DP_DOCUMENTS`.`DP_IS_ACTIVE`, `DP_DOCUMENTS`.`DP_FILE_NAME`, `DP_DOCUMENTS`.`DP_FILE_SIZE` ) ".
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ssssssiss", $docId, $clientId, $oriFilename, $date_str, 
                $date_str, $status, $active, $remoteFilename, $filesize)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
    }
    
    public function conversionComplete($docId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("UPDATE `DB_DocsPad`.`DP_DOCUMENTS` ".
                " SET `DP_CONVERSION_STATUS` = 'COMPLETED' ".
                " WHERE `DP_DOCUMENTS`.`DP_DOC_ID` = ?"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("s", $docId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }        
    }
    
    public function deleteDocument($clientId, $docId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("UPDATE `DB_DocsPad`.`DP_DOCUMENTS` ".
                " SET `DP_IS_ACTIVE` = '0' ".
                " WHERE `DP_DOCUMENTS`.`DP_DOC_ID` = ? AND ".
                "       `DP_DOCUMENTS`.`DP_CLIENT_ID` = ?"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ss", $docId, $clientId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt->store_result();
        $nrows = $stmt->affected_rows;
        
        if($nrows == 0){
            throw new \InvalidDocIdException("Invalid Doc Id", "302");
        }
    }
    
    public function checkStatus($clientId, $docId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("SELECT `DP_DOCUMENTS`.`DP_CONVERSION_STATUS`, ".
                "`DP_DOCUMENTS`.`DP_IS_ACTIVE`".
                " FROM`DB_DocsPad`.`DP_DOCUMENTS` ".
                " WHERE `DP_DOCUMENTS`.`DP_DOC_ID` = ? AND ".
                "       `DP_DOCUMENTS`.`DP_CLIENT_ID` = ?"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ss", $docId, $clientId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt->bind_result($status, $delete);
        
        $stmt->store_result();
        $nrows = $stmt->num_rows;
        
        if($nrows == 0){
            throw new \InvalidDocIdException("Invalid Doc Id", "302");
        }
        
        $stmt->fetch();
        
        if($delete == 0){
            $delete = "DELETED";
        } else{
            $delete = "PRESENT";
        }
        
        return array($status, $delete);
    }
    
    public function getDocumentOriginalName($clientId, $docId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("SELECT `DP_DOCUMENTS`.`DP_ORI_FILENAME`, "
                . "`DP_DOCUMENTS`.`DP_IS_ACTIVE` ".
                " FROM `DB_DocsPad`.`DP_DOCUMENTS` ".
                " WHERE `DP_DOCUMENTS`.`DP_DOC_ID` = ? AND ".
                "       `DP_DOCUMENTS`.`DP_CLIENT_ID` = ?"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ss", $docId, $clientId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt->bind_result($filename, $active);
        $stmt->store_result();
        $nrows = $stmt->num_rows;
        
        if($nrows == 0){
            throw new \InvalidDocIdException("Invalid Doc Id", "302");
        }
        
        //Fetch results for query
        $stmt->fetch();

        if($active == 0){
            throw new \DocDeletedException("Invalid Doc Id", "302");
        }
        
        return $filename;
    }
    
    public function getDocumentMetaData($clientId, $docId){
        $ob = $this->RDSBaseClassObject;
        $conn = $ob->con;
                        
        if (!($stmt = $conn->prepare("SELECT `DP_DOCUMENTS`.`DP_CREATED_DATE`, ".
                "`DP_DOCUMENTS`.`DP_FILE_SIZE`, `DP_DOCUMENTS`.`DP_FILE_NAME`, `DP_DOCUMENTS`.`DP_IS_ACTIVE` ".
                " FROM `DB_DocsPad`.`DP_DOCUMENTS` ".
                " WHERE `DP_DOCUMENTS`.`DP_DOC_ID` = ? AND ".
                "       `DP_DOCUMENTS`.`DP_CLIENT_ID` = ?"))) {
            echo "Prepare failed: (" . $conn->errno . ") " . $conn->error;
        }
        
        /* Prepared statement, stage 2: bind and execute */
        if (!$stmt->bind_param("ss", $docId, $clientId)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt->bind_result($createdDate, $filesize, $filename, $active);
        $stmt->store_result();
        $nrows = $stmt->num_rows;
        
        //TODO: This is not the correct way to check if the result is empty or not. Change it
//        if($nrows == 0){
//            throw new \InvalidDocIdException("Invalid Doc Id", "302");
//        }
        
        //Fetch results for query
        $stmt->fetch();

        if($active == 0){
            throw new \DocDeletedException("Invalid Doc Id", "302");
        }
        
        return array("filename" => $filename, "filesize" => $filesize, "created_date" => $createdDate);        
    }
}

?>
