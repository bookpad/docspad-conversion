<?php
include_once dirname(__FILE__).'/../plugins/PDFConverterPlugin.php';
include_once dirname(__FILE__).'/../plugins/DOCXConverterPlugin.php';
include_once dirname(__FILE__).'/../entities/FileTypes.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * It checks the extension of the file being uploaded and returns 
 * the correct plugin for the file
 *
 * @author niketh
 */
class PluginManager {
    public function getConverterPlugin($filename) {
        //Split the extension to find type of file
        $parts = explode(".", $filename);

        if(sizeof($parts) < 1){
            //@todo log this to file
            echo "File Extension does not exist";
            return;
        }
        
        $extension = strtolower($parts[sizeof($parts)-1]);        
        if($extension == FileTypes::PDF){
            $converter = new PDFConverterPlugin();
            return $converter;
        } elseif($extension == FileTypes::DOCX){
            $converter = new DOCXConverterPlugin();
            return $converter;
        } else{
            echo "Invalid extension";
            //Throw error an exception
        }
    }
}

?>