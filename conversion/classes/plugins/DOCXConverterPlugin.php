<?php
include_once dirname(__FILE__).'/../../tempwebsite/php/demo/DPConvertDOCX3.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DOCXConverterPlugin
 *
 * @author niketh
 */
class DOCXConverterPlugin extends ConverterPlugin {
    function __construct() {
    }
    
    public function convert($fileName, $filePath, $extractToDir, $convertedFile) {
        $inst = new DPConvertDOCX3($fileName, $filePath, $extractToDir, $convertedFile);
	$data = $inst->publishDPFormat();
	$inst->saveDataStructure();
        
        return $data;
    }
}

?>
