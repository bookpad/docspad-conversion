<?php

namespace DP;

use DP\FileTypes as FileTypes;
use DP\DocumentDefaults as DocumentDefaults;
use DP\DocumentMetaInfo as DocumentMetaInfo;
use DP\DocumentBody as DocumentBody;

require_once 'DPFileTypes.php';
require_once 'DPDocumentDefaults/DPDocumentDefaults.php';
require_once 'DPDocumentMetaInfo/DPDocumentMetaInfo.php';
require_once 'DPDocumentBody/DPDocumentBody.php';

class DPDocument{
    public $fileTypes;
    public $defaults;
    public $body;
    public $metaInfo;

    public function __construct($DOCType, $exist = false, $dataStructure = false){
        if($exist){

        }
        else{
            $this->fileTypes = new FileTypes\DPFileTypes();
            $this->defaults = new DocumentDefaults\DPDocumentDefaults();
            $this->metaInfo = new DocumentMetaInfo\DPDocumentMetaInfo();
            $this->body = new DocumentBody\DPDocumentBody();
            $this->metaInfo->documentType = $this->fileTypes->getConstant($DOCType);
        }
    }
    
    /*
    * prevents setting of class properties externally
    */
    public function __set($key, $val){
        return false;
    }
    
    /*
     * To set a default for the Document
     */
    
    public function setDocumentDefault($DPDocument, $defaultKey, $defaultVal){
        $type = gettype($defaultKey);
        if($type == 'array'){
            foreach($defaultKey as $key=>$val){
                $DPDocument->defaults->$key = $val;
            }
        }
        else if($type == 'string'){
            $DPDocument->defaults->$defaultKey = $defaultVal;
        }
        else{
            return false;
        }
    }
    
    /*
     * To set the metaInfo for Document
     * To add metakeys refer file DPMetaInfoKeys
     */
    
    public function setDocumentMetaInfo($DPDocument, $metaKey, $metaVal){
        $type = gettype($metaKey);
        if($type == 'array'){
            foreach($metaKey as $key=>$val){
                $DPDocument->metaInfo->$key = $val;
            }
        }
        else if($type == 'string'){
            $DPDocument->metaInfo->$metaKey = $metaVal;
        }
        else{
            return false;
        }
    }
    
    /*
     * To add a page node to the datastructure
     */
    
    public function addPage($DPDocument){
        $DPDocument->body->content->addPage();
    }
    
    /*
     * To set the body Metainfo
     * To add more keys just refer file DPBodyMetaKeys
     */
    
    public function setBodyMetaInfo($DPDocument, $metaKey, $metaVal){
        $type = gettype($metaKey);
        if($type == 'array'){
            foreach($metaKey as $key=>$val){
                $DPDocument->body->metaInfo->$key = $val;
            }
        }
        else if($type == 'string'){
            $DPDocument->body->metaInfo->$metaKey = $metaVal;
        }
        else{
            return false;
        }
    }
    
    /*
     * To add Node to a page
     */
    
    public function addNode($DPDocument){
        $bodycontent = $DPDocument->body->content;
        $bodycontent->pages[$bodycontent->pageCounter]->addNode();
    }
    
}

?>