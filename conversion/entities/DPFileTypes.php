<?php

namespace DP\FileTypes;

class DPFileTypes{
    const PDF = 'pdf';
    const XLS = 'excel';
    const DOCX = 'word';
    const PPT = 'ppt';
    
    public function getConstant($constKey) {
        return constant("self::".$constKey);
    }
}

?>