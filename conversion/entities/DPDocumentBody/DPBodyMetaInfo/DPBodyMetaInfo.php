<?php

namespace DP\DocumentBody\BodyMetaInfo;

class DPBodyMetaInfo{

    public function __construct() {
         
    }
    
    /*
    * prevents setting of class properties externally
    */
    public function __set($key, $val){
        return false;
    }
}

?>