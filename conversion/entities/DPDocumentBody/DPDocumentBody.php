<?php

namespace DP\DocumentBody;

use DP\DocumentBody\BodyMetaInfo as BodyMetaInfo;
use DP\DocumentBody\BodyContent as BodyContent;

include_once 'DPBodyMetaInfo/DPBodyMetaInfo.php';
include_once 'DPBodyContent/DPBodyContent.php';

class DPDocumentBody{
	public $metaInfo;
	public $content;
	
	public function __construct(){
            $this->metaInfo = new BodyMetaInfo\DPBodyMetaInfo();
            $this->content = new BodyContent\DPBodyContent();
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
}

?>