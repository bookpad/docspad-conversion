<?php

namespace DP\DocumentBody\BodyContent;

require_once('DPPageNode/DPPageNode.php');

class DPBodyContent{
	public $pages;
	public $pageCounter;
        public $imgCounter;
	
	public function __construct(){
            $this->pageCounter = 0;
            $this->imgCounter = 1;
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }

        public function addPage(){
            $this->pages[$this->pageCounter] = new PageNode\DPPageNode();
            $this->pageCounter++;
        }
}

?>