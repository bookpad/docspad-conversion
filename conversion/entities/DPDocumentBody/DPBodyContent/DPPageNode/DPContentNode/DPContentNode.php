<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode;

require_once 'DPContentNodeMetaInfo/DPContentNodeMetaInfo.php';
require_once 'DPContentNodeContent/DPContentNodeContent.php';
require_once 'DPTableNodeContent/DPTableNodeContent.php';
require_once 'DPTableNodeMetaInfo/DPTableNodeMetaInfo.php';

class DPContentNode{
	public $metaInfo;
	public $content;
	
	public function __construct($table){
                if(!$table){
                    $this->content = new ContentNodeContent\DPContentNodeContent();
                    $this->metaInfo = new ContentNodeMetaInfo\DPContentNodeMetaInfo();
                }
                else{
                    $this->content = new TableNodeContent\DPTableNodeContent();
                    $this->metaInfo = new TableNodeMetaInfo\DPTableNodeMetaInfo();
                    $this->metaInfo->type = 'table';
                }                    
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
	
}

?>