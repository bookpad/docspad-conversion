<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\TableNodeContent\TableColumn\TableCell\TableCellContent\ContentWord\WordMetaInfo;

class ImgMetaInfo{
    public $type;
    public $height;
    public $width;
    public $behindDoc;
    public $zIndex;
    public $marginTop;
    public $marginBottom;
    public $marginLeft;
    public $marginRight;
    public $posX;
    public $posY;
    public $referenceX;
    public $referenceY;
    
    /*
     * prevents setting of class properties externally
     */
    public function __set($key, $val){
        return false;
    }
}

?>