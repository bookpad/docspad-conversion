<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\TableNodeContent\TableColumn\TableCell\TableCellContent\ContentWord;

require_once 'DPContentWordMetaInfo/DPContentWordMetaInfo.php';
require_once 'DPContentWordContent/DPContentWordContent.php';

class DPContentWord{
	public $metaInfo;
	public $content;
	
	public function __construct(){
		$this->metaInfo = new WordMetaInfo\DPContentWordMetaInfo();
		$this->content = new WordContent\DPContentWordContent();
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
}


?>