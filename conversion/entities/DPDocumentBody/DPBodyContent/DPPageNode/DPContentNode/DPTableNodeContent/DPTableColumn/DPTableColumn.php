<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\TableNodeContent\TableColumn;

require_once 'DPTableCell/DPTableCell.php';
require_once 'DPRowMetaInfo/DPRowMetaInfo.php';


class DPTableColumn{
    public $column = array();
    public $metaInfo;
    public $columnCounter;
    
    public function __construct() {
        $this->columnCounter = 0;
        $this->metaInfo = new RowMetaInfo\DPRowMetaInfo();
    }
    
    public function addColumnToRow(){
        $this->column[$this->columnCounter] = new TableCell\DPTableCell();
    }
    
}

?>
