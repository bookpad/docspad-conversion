<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\TableNodeContent\TableColumn\TableCell\TableCellContent;

class DPTableCellContent{
    public $wordCounter;
    public $word = array();

    public function __construct(){
            $this->wordCounter = 0;
    }

    /*
    * prevents setting of class properties externally
    */
    public function __set($key, $val){
        return false;
    }

    public function addNode(){
            $this->word[$this->wordCounter] = new ContentWord\DPContentWord();
            $this->wordCounter++;
    }
}

?>
