<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\TableNodeContent\TableColumn\TableCell\TableCellContent\ContentWord\WordContent;

class DPContentWordContent{
	public $text;
	public $imgLink;
        
	/*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
}

?>