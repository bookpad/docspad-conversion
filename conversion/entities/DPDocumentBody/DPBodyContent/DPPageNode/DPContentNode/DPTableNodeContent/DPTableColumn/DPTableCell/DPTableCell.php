<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\TableNodeContent\TableColumn\TableCell;

require_once 'DPTableCellMetaInfo/DPTableCellMetaInfo.php';
require_once 'DPTableCellContent/DPTableCellContent.php';

class DPTableCell{
    public $metaInfo;
    public $content;
    
    public function __construct() {
        $this->metaInfo = new TableCellMetaInfo\DPTableCellMetaInfo();
        $this->content = new TableCellContent\DPTableCellContent();
    }
    
    
}

?>
