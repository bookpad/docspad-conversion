<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\TableNodeContent;

require_once 'DPTableColumn/DPTableColumn.php';

class DPTableNodeContent{
    public $rows = array();
    public $rowCounter;
    
    public function __construct() {
        $this->rowCounter = 0;
    }
    
    public function addRow(){
        $this->rows[$this->rowCounter] = new TableColumn\DPTableColumn();
    }
}

?>
