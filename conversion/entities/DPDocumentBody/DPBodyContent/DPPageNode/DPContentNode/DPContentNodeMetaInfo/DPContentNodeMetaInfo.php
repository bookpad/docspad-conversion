<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\ContentNodeMetaInfo;

include_once 'DPContentNodeEntities/ListMetaInfo.php';

class DPContentNodeMetaInfo{
        public $type;
	public $alignment;
        public $fontSize;
        public $fontColor;
        public $bold;
        public $italics;
	public $fontFamily;
        public $listMetaInfo;
        
	/*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
        
        public function initList(){
            $this->listMetaInfo = new ListMetaInfo();
        }
}


?>