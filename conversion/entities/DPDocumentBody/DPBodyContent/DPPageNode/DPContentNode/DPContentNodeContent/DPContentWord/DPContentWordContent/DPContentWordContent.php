<?php

namespace DP\DocumentBody\BodyContent\PageNode\ContentNode\ContentNodeContent\ContentWord\WordContent;

class DPContentWordContent{
	public $text;
	public $imgLink;
        
	/*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
}

?>