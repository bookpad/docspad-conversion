<?php

namespace DP\DocumentBody\BodyContent\PageNode;

require_once 'DPContentNode/DPContentNode.php';

class DPPageNode{
	public $elements = array();
	public $contentCounter;
	public $imgCounter;
	public $listCounter;
	public $paraCounter;
	
	public function __construct(){
            $this->contentCounter = 0;
            $this->imgCounter = 0;
            $this->listCounter = 0;
            $this->paraCounter = 0;
            $this->elements = array();
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
        
        public function addNode($table = FALSE){
            $this->elements[$this->contentCounter] = new ContentNode\DPContentNode($table);
            $this->contentCounter++;
	}

}
        
?>