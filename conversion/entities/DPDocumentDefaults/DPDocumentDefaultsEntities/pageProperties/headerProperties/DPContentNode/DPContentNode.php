<?php

include_once('DPContentNodeMetaInfo/DPContentNodeMetaInfo.php');
include_once('DPContentNodeContent/DPContentNodeContent.php');

class DPContentNode{
	public $metaInfo;
	public $content;
	
	public function __construct(){
		$this->metaInfo = new DPContentNodeMetaInfo();
		$this->content = new DPContentNodeContent();
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
	
}

?>