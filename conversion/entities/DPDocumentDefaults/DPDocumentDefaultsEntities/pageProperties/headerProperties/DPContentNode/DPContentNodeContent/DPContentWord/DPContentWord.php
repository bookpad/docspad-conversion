<?php

include_once('DPContentWordMetaInfo/DPContentWordMetaInfo.php');
include_once('DPContentWordContent/DPContentWordContent.php');

class DPContentWord{
	public $metaInfo;
	public $content;
	
	public function __construct(){
		$this->metaInfo = new DPContentWordMetaInfo();
		$this->content = new DPContentWordContent();
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
}


?>