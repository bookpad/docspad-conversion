<?php

include_once 'DOCX/ImgMetaInfo.php';

class DPContentWordMetaInfo{
	public $bold;
        public $underline;
        public $italics;
        public $fontColor;
        public $fontFamily;
        public $fontSize;
        public $type;
        public $imageMetaInfo;
	
	/*
         * prevents setting of class properties externally
         */
        public function __set($key, $val){
            return false;
        }
        
        public function initImgMetaInfo(){
            $this->imageMetaInfo = new ImgMetaInfo();
        }
}

?>