<?php

include_once('DPContentWord/DPContentWord.php');

class DPContentNodeContent{
	public $wordCounter;
	public $word = array();
	
	public function __construct(){
		$this->wordCounter = 0;
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            return false;
        }
	
	public function addNode(){
		$this->word[$this->wordCounter] = new DPContentWord();
		$this->wordCounter++;
	}
}


?>