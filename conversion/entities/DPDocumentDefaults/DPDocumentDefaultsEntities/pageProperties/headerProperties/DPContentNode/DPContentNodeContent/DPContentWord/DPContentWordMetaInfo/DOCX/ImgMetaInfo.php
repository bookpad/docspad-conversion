<?php

class ImgMetaInfo{
    public $type;
    public $height;
    public $width;
    public $behindDoc;
    public $zIndex;
    public $marginTop;
    public $marginBottom;
    public $marginLeft;
    public $marginRight;
    public $posX;
    public $posY;
    public $referenceX;
    public $referenceY;
    
    /*
     * prevents setting of class properties externally
     */
    public function __set($key, $val){
        return false;
    }
}

?>