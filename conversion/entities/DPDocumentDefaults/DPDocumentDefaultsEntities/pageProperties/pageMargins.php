<?php

namespace DP\DocumentDefaults\PageProperties;

class pageMargins{
    public $top;
    public $right;
    public $bottom;
    public $left;
    public $header;
    public $footer;
    public $gutter;
    
    /*
    * prevents setting of class properties externally
    */
    public function __set($key, $val){
        throw new Exception('Proprety Does Not Exist');
    }
}

?>
