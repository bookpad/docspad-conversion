<?php

namespace DP\DocumentDefaults\PageProperties;

include_once 'pageColumns/col.php';

class pageColumns{
    public $num;
    public $equalWidth;
    public $cols = array();
    
    /*
    * prevents setting of class properties externally
    */
    public function __set($key, $val){
        throw new Exception('Proprety Does Not Exist');
    }
    
    public function addColumn($numcols){
        for($i = 0; $i < $numcols; $i++){
            $this->cols[$i] = new col();
        }
    }
    
    public function addColumnInfo($colno, $width, $spacing){
        $this->cols[$colno]->width = $width;
        $this->cols[$colno]->spacing = $spacing;
    }
}


?>
