<?php

namespace DP\DocumentDefaults\PageProperties;

include_once 'pageProperties/pageSize.php';
include_once 'pageProperties/pageMargins.php';
include_once 'pageProperties/pageColumns.php';
include_once 'pageProperties/pageBackground.php';

class pageProperties{
    public $pageSize;
    public $pageMargins;
    public $pageColumns;
    public $pageBackground;
    
    public function __construct() {
        $this->pageSize = new pageSize();
        $this->pageMargins = new pageMargins();
        $this->pageColumns = new pageColumns();
        $this->pageBackground = new pageBackground();
    }
    
    
    /*
    * prevents setting of class properties externally
    */
    public function __set($key, $val){
        throw new Exception('Proprety Does Not Exist');
    }
}

?>