<?php

namespace DP\DocumentDefaults;

include_once 'DPDocumentDefaultsEntities/pageProperties.php';
include_once 'DPDocumentDefaultsEntities/paragraphProperties.php';

class DPDocumentDefaults{
        public $pageProperties;
        public $paragraphProperties;
	
	/* !sets defaults */
	public function __construct(){
            $this->pageProperties = new PageProperties\pageProperties();
            $this->paragraphProperties = new ParagraphProperties\paragraphProperties();
	}
        
        /*
        * prevents setting of class properties externally
        */
        public function __set($key, $val){
            throw new Exception('Proprety Does Not Exist');
        }
}

?>