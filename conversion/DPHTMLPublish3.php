<?php

class DPHTMLPublish{
	
	/* !Properties */
	public $DPdataStructure;
	
	public function __construct($dataStructure){
		$this->DPdataStructure = $dataStructure;
	}
	
	public function publishHTML(){
		/* !datasructure declaration */
                $data = $this->DPdataStructure;

    //        echo "<pre>";
    //        
    //        print_r($data);
    //        
    //        echo "</pre>";

                /* !content */
                $content = $data->body->content->elements;



                $fontColor = $data->body->metaInfo->fontColor;
                $fontSize = $data->body->metaInfo->fontSize;

                $style = $this->publishDocPageCSS($data);

                $str = $style.'<div class="DocPage"><div>';

                /* !para extraction */
                foreach($content as $ele){
			//print_r($ele);
			$textalign = (gettype($ele->metaInfo->textAlign)=='NULL')?'text-align:left;':'text-align: '.$ele->metaInfo->textAlign.';';
			$fontsize = (gettype($ele->metaInfo->fontSize) == 'NULL')?'font-size: 11px;':'font-size: '.$ele->metaInfo->fontSize.';';
						
			switch($this->paraType($ele->metaInfo->type)){
				case 0:
					/* !words extraction */
					$str = $str.'<p style="'.$textalign.$fontsize.'">';
                                        
					foreach($ele->content->word as $word){
						//print_r($word);
						$color = (gettype($word->metaInfo->fontColor) == 'NULL')?'':'color: '.$word->metaInfo->fontColor.';';
						$size = (gettype($word->metaInfo->fontSize) == 'NULL')?'':'font-size: '.$word->metaInfo->fontSize.';';
						$bold = (gettype($word->metaInfo->bold) == 'NULL')?'':'font-weight: bold;';
						$underline = (gettype($word->metaInfo->underline) == 'NULL')?'':'text-decoration: underline;';
						$fonttype = (gettype($word->metaInfo->fontFamily) == 'NULL')?'':'font-family: '.$word->metaInfo->fontFamily.';';
						
						if($this->wordType($word->metaInfo->type) == 2){
							$link = $word->content->link;
							//$result = glob($link.".*");
							$wid = $word->metaInfo->imageWidth;
							$hei = $word->metaInfo->imageHeight;
							//print_r($result);
							$str = $str.'<img src="./php/'.$link.'.png" width="'.$wid.'" height="'.$hei.'" align="bottom" />';
						}
						else{
							$text = $word->content->text;
							
							if($text == ''){
								$text = '&nbsp;';
							}
							else{
								$text = str_replace(' ','&nbsp;&thinsp;',$text);
							}
							$str = $str.'<span style="'.$color.$size.$bold.$underline.$fonttype.'">'.$text.'</span>';
						}
					}
					$str = $str.'</p>';
						
					break;
					
				case 1:
					$temp = $ele->metaInfo->fontSize;
					if($temp == '0px'){
						$temp = '11px';
					}
					$str = $str.'<p style="font-size: '.$temp.';" ><br></p>';
					break;
			}
			
			
		}
		
		$str = $str.'</div></div>';
		
		return $str;
	}
	
	
	/* !publich css */
	public function publishDocPageCSS($data){
		$bgcolor = '';
		
		if($this->detecttype($data->body->metaInfo->backgroundColor) != 0){
			$bgcolor = $data->body->metaInfo->backgroundColor;
		}
		else{
			$bgcolor = $data->defaults->backgroundColor;
		}
		
		$pageWidth = $data->body->metaInfo->pageSizeWidth;		
		$pageHeight = $data->body->metaInfo->pageSizeHeight;
		$rightMargin = $data->body->metaInfo->pageMarginRight;
		$leftMargin = $data->body->metaInfo->pageMarginLeft;
		$topMargin = $data->body->metaInfo->pageMarginTop;
		$bottomMargin = $data->body->metaInfo->pageMarginBottom;
		$footerMargin = $data->body->metaInfo->pageMarginFooter;
		$headerMargin = $data->body->metaInfo->pageMarginHeader;
		
		$pageWidthAct = $pageWidth - ($rightMargin + $leftMargin);
		$pageHeightAct = $pageHeight - ( $bottomMargin + $topMargin + $footerMargin + $headerMargin );
		
		$lineheight = $data->body->metaInfo->docGridLinePitch;
		$lineheight = ($lineheight/20);
		
		$str = '<style>.DocPage{';
		$str = $str.'background: '.$bgcolor.';';
		$str = $str.';width:'.$pageWidth.'px;height:'.$pageHeight.'px;margin: auto;} .DocPage > div{ width: '.$pageWidthAct.'px; height: '.$pageHeightAct.'px;  margin-right: '.$rightMargin.'px; margin-left: '.$leftMargin.'px; padding-top: '.$topMargin.'px; padding-bottom: '.$bottomMargin.'px;  }</style>';
		
		return $str;
	}
	
	/* !word meta type */
	public function wordType($type){
		switch($type){
			case 'text':
				return 1;
				break;
				
			case 'image':
				return 2;
				break;
		}
	}
	
	/* !para type */
	public function paraType($type){
		if(gettype($type) == 'NULL'){
			return 0;
		}
		else{
			switch($type){
				case 'emptyline':
					return 1;
					break;
					
				case 'list':
					return 3;
					break;
			}
		}
	}
	
	/* !detect type */
	public function detecttype($ele){
		switch(gettype($ele)){
			case 'object':
				return 1;
				break;
			case 'array':
				return 2;
				break;
			case 'string':
				return 3;
				break;
			case 'NULL':
				return 0;
				break;
		}
	}
	
}

?>