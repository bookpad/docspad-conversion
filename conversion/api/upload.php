<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

require_once __DIR__.'/../classes/manager/DPDocumentManager.php';
require_once __DIR__.'/../classes/manager/DPClientManager.php';
require_once __DIR__.'/../classes/manager/S3Manager.php';
require_once __DIR__.'/../classes/utilities/PHPPost.php';
require_once __DIR__.'/../classes/utilities/RandomString.php';
require_once __DIR__.'/../classes/utilities/PDFConverter.php';
require_once __DIR__.'/../classes/utilities/ProcessDir.php';
require_once __DIR__.'/../classes/exceptions/InvalidAPIKeyException.php';
require_once __DIR__.'/../classes/exceptions/ExpiredAPIKeyException.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function write_callback($ch, $data) {
     return strlen($data);
}

if(!isset($_REQUEST['doc']) && !isset($_FILES["doc"])){
    $ex = new Exception("Document being uploaded cannot be invalid", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        ),
    ));
    exit(1);
}

if(isset($_REQUEST['key'])){
    $apikey = $_REQUEST['key'];
} else{
    $ex = new Exception("Api key cannot be invalid", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        ),
    ));
    exit(1);
}

if(isset($_REQUEST['child'])){
    $child = $_REQUEST['child'];
}

if(isset($_REQUEST['split-pages'])){
    $splitPage = $_REQUEST['split-pages'];
    if($splitPage != "1" && $splitPage != "0"){
        $ex = new Exception("Invalid value for argument split-pages", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
        ));
        exit(1);    
    }
}

//Authenticate and get the client id from the api key
$document = new \docspad\conversion\classes\manager\DPClientManager();
try{
    $clientId = $document->getClientId($apikey);
} catch(InvalidAPIKeyException $e){
    $ex = new Exception("API key being supplied is invalid", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
    ));
    exit(1);
} catch(ExpiredAPIKeyException $e){
    $ex = new Exception("API key being supplied has expired", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
    ));
    exit(1);    
}

$s3manager = new \docspad\conversion\classes\manager\S3Manager();

if(isset($_FILES["doc"]["size"])) {
    //use child if it is set
    if(!isset($child)){
        $docId = RandomString::generateRandomString(10);
    } else{
        $docId = $child;
    }
    
//    echo $docId;
    $extensionParts = explode(".", $_FILES["doc"]["name"]);
    $extension = "";
            
    if(sizeof($extensionParts) > 0){
        $extension = $extensionParts[sizeof($extensionParts) - 1];
    }
    
    $filename = $docId.".".$extension;
    
    if ($_FILES["doc"]["size"] / 1024 > 0){
        move_uploaded_file($_FILES["doc"]["tmp_name"],
        "/tmp/" . $filename);
        
        $filepath = "/tmp/".$filename;

        $oriFilename = $_FILES["doc"]["name"];
        
        //upload original file to S3
        $s3manager->uploadFileWithPath("docspad-files-sn", $oriFilename, $filepath);
        
        $filesize = filesize($filepath);
        //enter document details in the DB    
        $document = new \docspad\conversion\classes\manager\DPDocumentManager();
        $document->insertDocument($clientId, $docId, $oriFilename, $oriFilename, $filesize);
        
        $pdfFile = $docId.".pdf";
        $pdfPath = "/tmp/".$pdfFile;
        
        //Convert only if filetype is not pdf
        if(strtolower($extension) == "pdf"){
            
        } elseif(strtolower($extension) == "epub"){
            exec("xvfb-run /opt/calibre/ebook-convert ".$filepath." ".$pdfPath);
        } else {
            //TODO Start a new thread and upload the document
            $converter = new \docspad\conversion\classes\utilities\PDFConverter();
            $converter->convert($filepath);
        }
        
        //Convert pdf to html
        $htmlFile = $docId.".html";
        $htmlpath= "/tmp/".$htmlFile;
        
        //upload pdf file to S3
        $s3manager->uploadFileWithPath("docspad-files-sn", $pdfFile, $pdfPath);
        
        if(isset($splitPage) && ($splitPage == '1')){
            //Convert pdf to html
            exec("pdf2htmlEX --dest-dir /tmp --no-drm 1 --data-dir ../pdf2html --split-pages 1 ".
                    " --page-filename ".$docId."-%d.html ".$pdfPath);
        } else{
            //Convert pdf to html
            exec("pdf2htmlEX --dest-dir /tmp --no-drm 1 --data-dir ../pdf2html ".$pdfPath);
        }
        
        //update the DB to mark completion of conversion
        $document->conversionComplete($docId);
        
        $nPages = intval(trim(exec("ls /tmp | grep -i ".$docId."- | wc -l")));
        for($i=1; $i <= $nPages; $i++){
            $htmlFilePart = $docId."-".$i.".html";
            $htmlpartpath= "/tmp/".$htmlFilePart;
            //upload html file to S3
            $s3manager->uploadFileWithPath("docspad-files-sn", $htmlFilePart, $htmlpartpath);
        }
        
        //upload html file to s3
        $s3manager->uploadFileWithPath("docspad-files-sn", $htmlFile, $htmlpath);

        //Generate thumbnails
        $generateThumbnails = "gs -dPDFFitPage -dPARANOIDSAFER -dBATCH -dNOPAUSE ".
                "-dNOPROMPT -dMaxBitmap=500000000 -dAlignToPixels=0 -dGridFitTT=0 ".
                " -dDEVICEWIDTH=150 -dDEVICEHEIGHT=200 -dORIENT1=true -sDEVICE=jpeg ".
                " -dTextAlphaBits=4 -dGraphicsAlphaBits=4    ".
                " -sOutputFile=/tmp/".$docId."_%d.jpg -q /tmp/".$docId.".pdf -c quit;";
        exec($generateThumbnails);
        
        //Upload thumbnails to S3
        $nThumbnails = exec("ls /tmp | grep -i ".$docId."_ | wc -l");
        for($i = 1; $i <= $nThumbnails; $i++){
            $imageFileName = $docId."_".$i.".jpg";
            $imageFilePath = "/tmp/".$imageFileName;
            //upload thubmnails to S3
            $s3manager->uploadFileWithPath("docspad-files-sn", $imageFileName, $imageFilePath);
        }
        
        echo $docId;
    }
    exit(1);
}
 else{
    $pattern = '/^(?:[;\/?:@&=+$,]|(?:[^\W_]|[-_.!~*\()\[\] ])|(?:%[\da-fA-F]{2}))*$/';
    $doc = $_REQUEST['doc'];
    
    //TODO This is a hack
    //Creating a hack that if the $_REQUEST array is greater than 2 then append remaining elements
    if(sizeof($_REQUEST) > 2){
        $requestsArray = $_REQUEST;
        unset($requestsArray['key']);
        unset($requestsArray['split-pages']);
//        if($requestsArray['key'] == "FuelKRvLxMDOrFRW"){
//            unset($requestsArray['key']);
//        }
        unset($requestsArray["SQLiteManager_currentLangue"]);
                
        $doc = "";
        foreach($requestsArray as $key => $value){
            if($key != 'doc'){
                $doc .= "&".$key;
                $value = str_replace("=", "%3D", $value);
                $doc .= "=".$value;
            } else{
                $doc .= $value;
            }
            
        }
    }
        
    $doc = str_replace(" ", "%20", $doc);
    
    if( preg_match( $pattern, $doc ) == 1 ) {
        //TODO Handle only certain type of documents.
        //Have filters here
        $extension = explode(".", $doc);      
        $extension = $extension[sizeof($extension)-1];
        if($extension == ""){
            exit(1);
        }
        
        //Some urls have ?. Remove it
        $tempExt = explode("?", $extension);
        if(sizeof($tempExt) > 0){
            $extension = $tempExt[0];
        }
                
        $docId = RandomString::generateRandomString(10);

        $remoteFilename = exec("curl -sI  '".$doc."' | grep -o -E 'filename=.*$' | sed -e 's/filename=//'");
        $remoteFilename = str_replace('"', "", $remoteFilename);
        $remoteExt = explode(".", $remoteFilename);
        $remoteExt = array_pop($remoteExt);
        
        if($remoteFilename == ""){
            $remoteFilename = explode("/", $doc);
            $remoteFilename = array_pop($remoteFilename);
        }
        
        if($remoteExt != ""){
            $extension = $remoteExt;
            $filename = $docId.".".$remoteExt;
        } else{
            $filename = $docId.".".$extension;
        }

        $filepath = "/tmp/".$filename;

        
        //If the doc contains public doc download it
//        $downloadCmd = "curl -L ".$doc." -o ".$filepath." && cat ".$filepath;
        $downloadCmd = "curl -L '".$doc."' -o ".$filepath;
        $contents = exec($downloadCmd);
        
        $filesize = filesize($filepath);
        $document = new \docspad\conversion\classes\manager\DPDocumentManager();
        //enter document details in the DB
        $document->insertDocument($clientId, $docId, $doc, $remoteFilename, $filesize);
        
        if($extension == 'zip'){
            $zipFileContents = "/tmp/".$docId;
            //unzip the files
            $zip = new ZipArchive;
            if ($zip->open($filepath) === TRUE) {
                $zip->extractTo($zipFileContents);
                $zip->close();
            } else {
                $ex = new Exception("ERROR: ZIP files cannot be unzipped", "302");
                echo json_encode(array(
                        'error' => array(
                        'msg' => $ex->getMessage(),
                        'code' => $ex->getCode(),
                        ),
                ));
                exit(1);
            }
            
            $childCnt = 0;
            $processDir = new \ProcessDir();
            $list = $processDir->process_dir($zipFileContents, TRUE);
            $docIdsArray = array();
                
            $url = "http://api.docspad.com/upload.php";
                
            $childCnt = 0;
                
            while($childCnt < sizeof($list))
            {    
                $process_count = 5;
                $mh = curl_multi_init();
                $handles = array();
            
                while ($process_count-- && $childCnt < sizeof($list))
                {
                    $child = $docId.'_'.$childCnt;
                    $entry = $list[$childCnt]['dirpath']."/".$list[$childCnt]['filename'];
                    $entry = realpath($entry);
//                    echo "Entry: ".$entry.'<br/>';
//                    echo "Child: ".$child.'<br/>';
                    $postData = array("doc" => '@'.$entry, "key" => $apikey, "child" => $child);
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, count($postData));
                    curl_setopt($ch, CURLOPT_WRITEFUNCTION, 'write_callback');
                    curl_setopt ($ch, CURLOPT_POSTFIELDS, $postData);
                    curl_multi_add_handle($mh, $ch);
                    $handles[] = $ch;
                    
                    array_push($docIdsArray, $child);
                    $childCnt++;
                }

                $running=null;

                do {
                    curl_multi_exec($mh, $running);
                } 
                while ($running > 0);

                for($i = 0; $i < count($handles); $i++){
                    $out = curl_multi_getcontent($handles[$i]);
                    curl_multi_remove_handle($mh, $handles[$i]);
                }

                curl_multi_close($mh);
            }
        
            //update the DB to mark completion of conversion
            $document->conversionComplete($docId);
            echo json_encode(array("docId" => $docId, "childIds" => $docIdsArray));
            exit(1);
        }
        
        //upload original file to S3
        $s3manager->uploadFileWithPath("docspad-files-sn", $filename, $filepath);
        
        $pdfFile = $docId.".pdf";
        $pdfPath = "/tmp/".$pdfFile;
        
        //Convert only if filetype is not pdf
        if(strtolower($extension) != "pdf"){
            //TODO Start a new thread and upload the document
            $converter = new \docspad\conversion\classes\utilities\PDFConverter();
            $converter->convert($filepath);
        } elseif(strtolower($extension) != "epub"){
            exec("xvfb-run /opt/calibre/ebook-convert ".$filepath." ".$pdfPath);
        }
        
        //Convert pdf to html
        $htmlFile = $docId.".html";
        $htmlpath= "/tmp/".$htmlFile;
        
        //upload pdf file to S3
        $s3manager->uploadFileWithPath("docspad-files-sn", $pdfFile, $pdfPath);

        if(isset($splitPage) && ($splitPage == '1')){
            //Convert pdf to html
            exec("pdf2htmlEX --dest-dir /tmp --no-drm 1 --data-dir ../pdf2html --split-pages 1 ".
                    " --page-filename ".$docId."-%d.html ".$pdfPath);
        } else{
            //Convert pdf to html
            exec("pdf2htmlEX --dest-dir /tmp --no-drm 1 --data-dir ../pdf2html ".$pdfPath);
        }
        
        //update the DB to mark completion of conversion
        $document->conversionComplete($docId);
        
        $nPages = intval(trim(exec("ls /tmp | grep -i ".$docId."- | wc -l")));
        
        for($i=1; $i <= $nPages; $i++){
            $htmlFilePart = $docId."-".$i.".html";
            $htmlpartpath= "/tmp/".$htmlFilePart;
            //upload html file to S3
            $s3manager->uploadFileWithPath("docspad-files-sn", $htmlFilePart, $htmlpartpath);
        }
        
        //upload html file to S3
        $s3manager->uploadFileWithPath("docspad-files-sn", $htmlFile, $htmlpath);
        
        
        //Generate thumbnails
        $generateThumbnails = "gs -dPDFFitPage -dPARANOIDSAFER -dBATCH -dNOPAUSE ".
                "-dNOPROMPT -dMaxBitmap=500000000 -dAlignToPixels=0 -dGridFitTT=0 ".
                " -dDEVICEWIDTH=150 -dDEVICEHEIGHT=200 -dORIENT1=true -sDEVICE=jpeg ".
                " -dTextAlphaBits=4 -dGraphicsAlphaBits=4    ".
                " -sOutputFile=/tmp/".$docId."_%d.jpg -q /tmp/".$docId.".pdf -c quit;";
        exec($generateThumbnails);
        
        //Upload thumbnails to S3
        $nThumbnails = exec("ls /tmp | grep -i ".$docId."_ | wc -l");
        for($i = 1; $i <= $nThumbnails; $i++){
            $imageFileName = $docId."_".$i.".jpg";
            $imageFilePath = "/tmp/".$imageFileName;
            //upload thubmnails to S3
            $s3manager->uploadFileWithPath("docspad-files-sn", $imageFileName, $imageFilePath);
        }
        
        //delete file from tmp
        exec("rm -rf ".$docId."*&");
        
        echo $docId;
        exit(1);
    } else{
        $ex = new Exception("Url being passed is not valid", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
        ));
        exit(1);
     }
 }
 

?>
