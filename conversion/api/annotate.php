<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once __DIR__.'/../classes/manager/DPDocumentManager.php';
require_once __DIR__.'/../classes/manager/DPClientManager.php';
require_once __DIR__.'/../classes/manager/DPNotesManager.php';
require_once __DIR__.'/../classes/manager/S3Manager.php';
require_once __DIR__.'/../classes/utilities/PHPPost.php';
require_once __DIR__.'/../classes/utilities/RandomString.php';
require_once __DIR__.'/../classes/utilities/PDFConverter.php';
require_once __DIR__.'/../classes/utilities/ProcessDir.php';
require_once __DIR__.'/../classes/exceptions/InvalidAPIKeyException.php';
require_once __DIR__.'/../classes/exceptions/ExpiredAPIKeyException.php';
    
if(isset($_REQUEST['key'])){
    $apikey = $_REQUEST['key'];
} else{
    $ex = new Exception("Api key cannot be invalid", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        )
    ));
    exit(1);
}

if(isset($_REQUEST['option'])){
    $option = $_REQUEST['option'];
} else{
    $ex = new Exception("option cannot be empty", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        )
    ));
    exit(1);
}

if(isset($_REQUEST['posX'])){
    $posX = $_REQUEST['posX'];
} else{
    $posX = 0;
}

if(isset($_REQUEST['posY'])){
    $posY = $_REQUEST['posY'];
} else{
    $posY = 0;
}

if($option != "ADD" && $option != "DELETE" && $option != "EDIT" && $option != "GET"){
    $ex = new Exception("option being supplied is not supported", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        )
    ));
    exit(1);    
}

    //Authenticate and get the client id from the api key
    $document = new \docspad\conversion\classes\manager\DPClientManager();
    try{
        $clientId = $document->getClientId($apikey);
    } catch(InvalidAPIKeyException $e){
        $ex = new Exception("API key being supplied is invalid", "302");
        echo json_encode(array(
                'error' => array(
                'msg' => $ex->getMessage(),
                'code' => $ex->getCode(),
                )
        ));
        exit(1);
    } catch(ExpiredAPIKeyException $e){
        $ex = new Exception("API key being supplied has expired", "302");
        echo json_encode(array(
                'error' => array(
                'msg' => $ex->getMessage(),
                'code' => $ex->getCode(),
                ),
        ));
        exit(1);    
    }

$notesManager = new \docspad\conversion\classes\manager\DPNotesManager();
    
if($option == "ADD"){
    if(isset($_REQUEST['doc'])){
        $docId = $_REQUEST['doc'];
    } else {
        $ex = new Exception("Document Identifier cannot be invalid", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
        ));
        exit(1);
    }
    
    if(isset($_REQUEST['page'])){
        $page = $_REQUEST['page'];
    } else{
        $ex = new Exception("page cannot be empty", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
        ));
        exit(1);
    }
    
    if(isset($_REQUEST['content'])){
        $content = $_REQUEST['content'];
    } else{
        $ex = new Exception("content has to be set, it cannot be null", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
        ));
        exit(1);
    }
    
    //Generate random noteid
    $noteId = RandomString::generateRandomString(32);
    
    //Add note to DB
    $notesManager->insertNote($clientId, $docId, $noteId, $page, $content, $posX, $posY);
    
    echo json_encode(array(
        "noteId" => $noteId
            ));
    exit(1);
} elseif($option == "GET"){
    if(isset($_REQUEST['note'])){
        $noteId = $_REQUEST['note'];
    } elseif($_REQUEST['doc']){
        $docId = $_REQUEST['doc'];
    } else{
        $ex = new Exception("content has to be set, it cannot be null", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
        ));
        exit(1);        
    }
    
    if(isset($noteId)){
        $noteInfo = $notesManager->getNote($clientId, $noteId);
        echo json_encode($noteInfo);
    } elseif(isset($docId)){
        $notesInfo = $notesManager->getAllNotes($clientId, $docId);
        echo json_encode($notesInfo);
    }
} elseif($option == "EDIT"){
    if(isset($_REQUEST['note'])){
        $noteId = $_REQUEST['note'];
    } else{
        $ex = new Exception("NoteId being passed cannot be null", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
        ));
        exit(1);         
    }
    
    if(isset($_REQUEST['content'])){
        $contents = $_REQUEST['content'];
    } else{
        $ex = new Exception("Content being passed cannot be null", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
        ));
        exit(1);         
    }
    
    $editStatus = $notesManager->editNote($clientId, $noteId, $contents);
    echo json_encode(array(
        "status" => $editStatus
    ));    
} elseif($option == "DELETE"){
    if(isset($_REQUEST['note'])){
        $noteId = $_REQUEST['note'];
    } else{
        $ex = new Exception("NoteId being passed cannot be null", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
        ));
        exit(1);         
    }
    
    $deleteStatus = $notesManager->deleteNote($clientId, $noteId);
    echo json_encode(array(
        "status" => $deleteStatus
    ));    
}
?>
