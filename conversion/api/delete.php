<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once __DIR__.'/../classes/manager/DPDocumentManager.php';
require_once __DIR__.'/../classes/manager/DPClientManager.php';
require_once __DIR__.'/../classes/exceptions/InvalidDocIdException.php';
require_once __DIR__.'/../classes/exceptions/InvalidAPIKeyException.php';
require_once __DIR__.'/../classes/exceptions/ExpiredAPIKeyException.php';

if(isset($_REQUEST['doc'])){
    $docId = $_REQUEST['doc'];
} else {
    $ex = new Exception("Document Identifier cannot be invalid", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        ),
    ));
    exit(1);
}

if(isset($_REQUEST['key'])){
    $apikey = $_REQUEST['key'];
} else{
    $ex = new Exception("Api key cannot be invalid", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        ),
    ));
    exit(1);
}

//Authenticate and get the client id from the api key
$document = new \docspad\conversion\classes\manager\DPClientManager();
try{
    $clientId = $document->getClientId($apikey);
} catch(InvalidAPIKeyException $e){
    $ex = new Exception("API key being supplied is invalid", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
    ));
    exit(1);
} catch(ExpiredAPIKeyException $e){
    $ex = new Exception("API key being supplied has expired", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
    ));
    exit(1);    
}

//Delete document from DB    
$document = new \docspad\conversion\classes\manager\DPDocumentManager();
try{
    $document->deleteDocument($clientId, $docId);
} catch(InvalidDocIdException $e){
    $ex = new Exception("Doc id being passed is invalid", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
    ));
    exit(1);
}

?>
