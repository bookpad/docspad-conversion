<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once __DIR__.'/../classes/manager/DPDocumentManager.php';
require_once __DIR__.'/../classes/manager/DPAPILogManager.php';
require_once __DIR__.'/../classes/manager/DPClientManager.php';
require_once __DIR__.'/../classes/manager/S3Manager.php';
require_once __DIR__.'/../classes/exceptions/InvalidDocIdException.php';
require_once __DIR__.'/../classes/exceptions/InvalidAPIKeyException.php';
require_once __DIR__.'/../classes/exceptions/DocDeletedException.php';
require_once __DIR__.'/../classes/exceptions/ExpiredAPIKeyException.php';

//class APIDownloadLog extends Thread {
//    public function run($clientId, $docId) {
//        
//    }
//}

if(isset($_REQUEST['doc'])){
    $docId = $_REQUEST['doc'];
} else {
    $ex = new Exception("Document Identifier cannot be invalid", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        )
    ));
    exit(1);
}

if(isset($_REQUEST['key'])){
    $apikey = $_REQUEST['key'];
} else{
    $ex = new Exception("Api key cannot be invalid", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        )
    ));
    exit(1);
}

//Authenticate and get the client id from the api key
$document = new \docspad\conversion\classes\manager\DPClientManager();
try{
    $clientId = $document->getClientId($apikey);
} catch(InvalidAPIKeyException $e){
    $ex = new Exception("API key being supplied is invalid", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
    ));
    exit(1);
} catch(ExpiredAPIKeyException $e){
    $ex = new Exception("API key being supplied has expired", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
    ));
    exit(1);    
}

//If original = 1 then get back original document
//else return html document
if(isset($_REQUEST['original'])){
    $original = $_REQUEST['original'];
    if($original !=0 && $original != 1 && $original != "PDF"){
        $ex = new Exception("Arguments for original are invalid", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
        ));
        exit(1);
    }
}else{
    $original = 0;
}

$pageId = $docId;
//You might want to give more advanced options to get pages using regex, 
//comma separated, page ranges etcs
if(isset($_REQUEST['page'])){
    $pages = $_REQUEST['page'];

    //TODO handle the case where the page no is not integer
    //if(isset($pages)){
    //    $ex = new Exception("Page no being supplied is not valid", "302");
    //    echo json_encode(array(
    //        'error' => array(
    //        'msg' => $ex->getMessage(),
    //        'code' => $ex->getCode(),
    //        )
    //    ));
    //    exit(1);
    //}
    
    //If $pages is set, then modify the $docId to the new pageId
    $pageId = $docId."-".$pages;
}



try{
    //Delete document from DB    
    $document = new \docspad\conversion\classes\manager\DPDocumentManager();
    $ori_filename = $document->getDocumentOriginalName($clientId, $docId);
} catch(InvalidDocIdException $e){
    $ex = new Exception("Doc id being passed is invalid", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
    ));
    exit(1);
} catch(DocDeletedException $e){
    $ex = new Exception("Doc with this id has been deleted", "302");
    echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            )
    ));
    exit(1);    
}

$s3manager = new \docspad\conversion\classes\manager\S3Manager();

if($original == "1"){
    $extension = explode(".", $ori_filename);
    $extension = $extension[sizeof($extension)-1];    
} elseif($original == "0"){
    $extension = "html";
} elseif($original == "PDF"){
    $extension = "pdf";
}

$document = new \docspad\conversion\classes\manager\DPAPILogManager();
$document->insertDownloadLog($clientId, $pageId);
        
$s3manager->readObject('docspad-files-sn', $pageId.'.'.$extension);

?>
