<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('max_execution_time', 300); //300 seconds = 5 minutes

require_once __DIR__.'/../classes/manager/DPDocumentManager.php';
require_once __DIR__.'/../classes/manager/DPClientManager.php';
require_once __DIR__.'/../classes/manager/S3Manager.php';
require_once __DIR__.'/../classes/utilities/PHPPost.php';
require_once __DIR__.'/../classes/utilities/RandomString.php';
require_once __DIR__.'/../classes/utilities/PDFConverter.php';
require_once __DIR__.'/../classes/utilities/ProcessDir.php';
require_once __DIR__.'/../classes/exceptions/InvalidAPIKeyException.php';
require_once __DIR__.'/../classes/exceptions/ExpiredAPIKeyException.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(isset($_REQUEST['doc'])){
    $docId = $_REQUEST['doc'];
} else {
    $ex = new Exception("Document Identifier cannot be invalid", "302");
    echo json_encode(array(
        'error' => array(
        'msg' => $ex->getMessage(),
        'code' => $ex->getCode(),
        )
    ));
    exit(1);
}

//Check if session_id is being passed
if(isset($_REQUEST['session_id'])){
    $sessionId = $_REQUEST['session_id'];
} else{
    if(isset($_REQUEST['key'])){
        $apikey = $_REQUEST['key'];
    } else{
        $ex = new Exception("Api key cannot be invalid", "302");
        echo json_encode(array(
            'error' => array(
            'msg' => $ex->getMessage(),
            'code' => $ex->getCode(),
            ),
        ));
        exit(1);
    }
}

$clientId = "";
//$sessionManager = new \docspad\conversion\classes\manager\DPSessionManager();
$document = new \docspad\conversion\classes\manager\DPClientManager();

//if session_id is set then use it to get client info
if(isset($sessionId)){
    $clientId = $sessionManager->getClientId($sessionId);
} else{
    //Authenticate and get the client id from the api key
    try{
        $clientId = $document->getClientId($apikey);
    } catch(InvalidAPIKeyException $e){
        $ex = new Exception("API key being supplied is invalid", "302");
        echo json_encode(array(
                'error' => array(
                'msg' => $ex->getMessage(),
                'code' => $ex->getCode(),
                ),
        ));
        exit(1);
    } catch(ExpiredAPIKeyException $e){
        $ex = new Exception("API key being supplied has expired", "302");
        echo json_encode(array(
                'error' => array(
                'msg' => $ex->getMessage(),
                'code' => $ex->getCode(),
                ),
        ));
        exit(1);    
    }
}

$documentManager = new \docspad\conversion\classes\manager\DPDocumentManager();
try{
    $metaData = $documentManager->getDocumentMetaData($clientId, $docId);
} catch(\Exception $e){
    
}

echo json_encode($metaData);

?>
