<?php

/*
 * dependencies
 */
include_once('entities/DPDocument.php');

class DPConvertDOCX{
    /*
     * docx file name
     */
    public $fileName;
    
    /*
     * file path relative to the location of the class file
     */
    public $filePath;
    
    /*
     * file extraction directory
     */
    public $extractToDir;
    
    /*
     * converted data structure
     */
    public $convertedFile;
    
    /*
     * complete converted data into a organised structure
     */
    public $dataStructure;
    
    public $docNameSpaces;
    
    public $targetArr;
    
    /*
     * path to main document
     */
    private $mainDocument;
    
    /*
     * directory for main document
     */
    private $mainDocumentDir;
    
    public $documentRelInfo;
    
    public $listIdentifier;
    
    public $relsInfo;
    
    public $wordRelsInfo;

    public function __construct($fileName, $filePath, $extractToDir, $convertedFile) {
        $this->fileName = $fileName;
        $this->filePath = $filePath;
        $this->extractToDir = $extractToDir;
        $this->convertedFile = $convertedFile;
        if(is_dir($this->convertedFile)){
            $this->deleteFolder($this->convertedFile);
        }
        mkdir($this->convertedFile);
        
        $relsarr = $this->getRelsInfo();
        $this->relsInfo = $this->getRelsInfo();
        $this->getSchemasTarget($this->relsInfo);
        $this->wordRelsInfo = $this->getWordRelsInfo();
    }
    
    /*
     * register specific custom namespaces
     */
    public function registerCustomNameSpaces(){
        $this->docNameSpaces['a'] = 'http://schemas.openxmlformats.org/drawingml/2006/main';
        $this->docNameSpaces['pic'] = 'http://schemas.openxmlformats.org/drawingml/2006/picture';
    }
	
    /*
     * pixel to emu
     */
    public function EMU2pixel($EMU){
        return $EMU/9525;
    }

    public function TwipsMeasure2Pixel($Twips){
        return $Twips/20;
    }

    /*
     * extracts the docx file to the mentioned directory
     */
    public function extractFile() {
        $zip = new ZipArchive();
        $res = $zip->open(($this->filePath).($this->fileName));

        if($res === TRUE){
                $zip->extractTo($this->extractToDir);
                $zip->close();
                return 1;
        }
        else{
                return 0;
        }
    }

    /*
     * extracts schemas, IDs and targets
     */
    public function getRelsInfo() {
        $rels = simplexml_load_file(($this->extractToDir).'_rels/.rels');
        if($rels){
                $data = array();
                $relationships = $rels->Relationship;
                foreach($relationships as $relationship){
                        $val = get_object_vars($relationship);
                        $data[$val['@attributes']['Id']] = array(
                            'Type' => $val['@attributes']['Type'],
                            'Target' => $val['@attributes']['Target'],
                        );
                }
                return $data;
        }
        else{
                return 0;
        }
    }
    
    /*
     * extracts word rels info
     */
    public function getWordRelsInfo() {
        $rels = simplexml_load_file(($this->extractToDir).'word/_rels/document.xml.rels');
        if($rels){
                $data = array();
                $relationships = $rels->Relationship;
                foreach($relationships as $relationship){
                        $val = get_object_vars($relationship);
                        $data[$val['@attributes']['Id']] = array(
                            'Type' => $val['@attributes']['Type'],
                            'Target' => $val['@attributes']['Target'],
                        );
                }
                return $data;
        }
        else{
                return 0;
        }
    }

    /*
     * get different schemas
     */
    public function getSchemasTarget($relsarr) {
        $data = array();
        foreach($relsarr as $rel){
                $temp = $rel['Type'];
                $temparr = preg_split('/\//', $temp);
                $data[$temparr[sizeof($temparr) - 1]] = $rel['Target'];
        }
        $this->targetArr = $data;
    }

    /*
     * extract text
     */
    public function mainDocument() {
        $fpxml = simplexml_load_file(($this->extractToDir).$this->targetArr['officeDocument']);
        return $fpxml;
    }

    /*
     * extract all doc namespaces
     */
    public function getDOCXNameSpaces($xmlhandle){
        $this->docNameSpaces = $xmlhandle->getDocNamespaces();
    }

    /*
     * extract fontTable
     */
    public function wordRelInfo() {
        $str = $this->targetArr['officeDocument'];
        $str = preg_split('/\//', $str);
        $this->mainDocumentDir = $str[0];

        $fpxml = simplexml_load_file(($this->extractToDir).$str[0].'/_rels/document.xml.rels');
        $data = array();
        $relationships = $fpxml->Relationship;
        foreach($relationships as $relationship){
                $val = get_object_vars($relationship);
                $temp = $val['@attributes'];
                $type = $temp['Type'];
                $temparr = preg_split('/\//', $type);
                $data[$temparr[sizeof($temparr) - 1]] = $temp['Target'];
        }
        $this->documentRelInfo = $data;
    }
    
    /*
     * extracts the document background properties
     */
    public function documentBackground($DPDocument, $body){
        $bg = $this->convert2Array($body->background);
        
        /*
         * documentr background color
         */
        
        $DPBackground = $DPDocument->defaults->pageProperties->pageBackground;
        
        $bgcolor = '#'.$bg['@attributes']['color'];
        
        if($this->detecttype($bgcolor) == 0){
            $bgcolor = '#FFFFFF';
        }
        
        $DPBackground->color = $bgcolor;
    }
    
    /*
     * page properties of the document
     */
    public function documentPageProperties($DPDocument, $body){
        $sect = $body->body->sectPr;
        
        /*
         * columns
         */
        $cols = $this->convert2Array($sect->cols);
        
        $DPPageColumns = $DPDocument->defaults->pageProperties->pageColumns;
        
        $numcols = $cols['@attributes']['num'];
        if($this->detecttype($numcols) == 0){
            $DPPageColumns->num = 1;
        }
        else{
            $DPPageColumns->num = $numcols;
            
            if(key_exists('col', $cols)){
                $DPPageColumns->addColumn($numcols);
                $colsinfo = $cols['col'];
                foreach($colsinfo as $k=>$col){
                    $col = $this->convert2Array($col);
                    $space = ($this->detecttype($col['@attributes']['space']) == 0)?0:$col['@attributes']['space'];
                    $DPPageColumns->addColumnInfo($k, $col['@attributes']['w'], $space);
                }
            }
            else{
                $DPPageColumns->equalWidth = 1;
            }
            
        }
        
        /*
         * page margins
         */
        $pagemargins = $this->convert2Array($sect->pgMar);
        
        $DPPageMargins = $DPDocument->defaults->pageProperties->pageMargins;
        
        $DPPageMargins->top = $pagemargins['@attributes']['top'];
        $DPPageMargins->right = $pagemargins['@attributes']['right'];
        $DPPageMargins->bottom = $pagemargins['@attributes']['bottom'];
        $DPPageMargins->left = $pagemargins['@attributes']['left'];
        $DPPageMargins->header = $pagemargins['@attributes']['header'];
        $DPPageMargins->footer = $pagemargins['@attributes']['footer'];
        $DPPageMargins->gutter = $pagemargins['@attributes']['gutter'];
        
        /*
         * page size
         */
        $pagesize = $this->convert2Array($sect->pgSz);
        
        $DPDocument->defaults->pageProperties->pageSize->width = $pagesize['@attributes']['w'];
        $DPDocument->defaults->pageProperties->pageSize->height = $pagesize['@attributes']['h'];
        
        /*
         * headers
         */
//        foreach ($sect->headerReference as $header){
//            $headerref = $header->attributes($this->docNameSpaces['r']);
//            $headerref = $this->convert2Array($headerref);
//            $header = $this->convert2Array($header);
//            
//            if($header['@attributes']['type'] == 'first'){
//                $this->documentHeaderInfo($DPDocument, $body, $this->extractToDir.'word/'.$this->wordRelsInfo[$headerref['@attributes']['id']]['Target'], $header['@attributes']['type']);
//            }
//            else if($header['@attributes']['type'] == 'default'){
//                $this->documentHeaderInfo($DPDocument, $body, $this->extractToDir.'word/'.$this->wordRelsInfo[$headerref['@attributes']['id']]['Target'], $header['@attributes']['type']);
//                
//            }
//            else if($header['@attributes']['type'] == 'even'){
//                $this->documentHeaderInfo($DPDocument, $body, $this->extractToDir.'word/'.$this->wordRelsInfo[$headerref['@attributes']['id']]['Target'], $header['@attributes']['type']);
//                
//            }
//        }
        
    }
    
    /*
     * document header extraction
     */
    public function documentHeaderInfo($DPDocument, $body, $fp, $type){
        $fheader = simplexml_load_file($fp);
        $pheader = $fheader->children($this->docNameSpaces['w']);
        
        foreach ($pheader as $p){
            
        }
    }
    
    /*
     * document Paragraph Defaults
     */
    public function documentParagraphDefaults($DPDocument, $body){
        $sect = $body->body->sectPr;
        
        $doc = $this->convert2Array($sect->docGrid);
        
        $DPDocument->defaults->paragraphProperties->linePitch = $doc['@attributes']['linePitch'];
    }
    
    /*
     * get list bullet properties
     */
    public function listBulletProperties(){
        
    }
    
    /*
     * document individual paragraph properties extraction
     */
    public function extractParagraphProperties($DPDocument, $p){
        $pPr = $p->pPr;
        
        if($this->detecttype($pPr) != 0){
            $pageCounter = ($DPDocument->body->content->pageCounter) - 1;
            
            $DPDocument->body->content->pages[$pageCounter]->addNode();
            
            $paraCounter = ($DPDocument->body->content->pages[$pageCounter]->contentCounter) - 1;

            $paraMetaInfo = $DPDocument->body->content->pages[$pageCounter]->elements[$paraCounter]->metaInfo;
            
            /*
             * list detection
             */
            $list = $this->convert2Array($pPr->pStyle);
            if($this->detecttype($list) != 0){
                if($list['@attributes']['val'] != 'ListParagraph'){
                    $this->listIdentifier = 0;
                }
                else{
                    $this->listIdentifier = 1;
                    $paraMetaInfo->type = 'list';
                    $paraMetaInfo->initList();
                    $lvl = $this->convert2Array($pPr->numPr->ilvl);
                    $numid = $this->convert2Array($pPr->numPr->numId);
                    $numid = $numid['@attributes']['val'];
                    $paraMetaInfo->listMetaInfo->level = $lvl['@attributes']['val'];
                }
            }else{
                $this->listIdentifier = 0;
            }
            
            /*
             * paragraph alignment
             */
            $jc = $this->convert2Array($pPr->jc);
            
            if($this->detecttype($jc)){
                $paraMetaInfo->alignment = $jc['@attributes']['val'];
            }
            
            $rPr = $this->convert2Array($pPr->rPr);
            
            if($this->detecttype($rPr)){
                /*
                 * font size
                 */
                $sz = $this->convert2Array($rPr['sz']);
                if($this->detecttype($sz)){
                    $paraMetaInfo->fontSize = $sz['@attributes']['val']/2;
                }
                
                /*
                 * bold
                 */
                $b = $rPr['b'];
                if($this->detecttype($b)){
                    $paraMetaInfo->bold = 1;
                }
                
                /*
                 * italics
                 */
                $i = $rPr['i'];
                if($this->detecttype($i)){
                    $paraMetaInfo->italics = 1;
                }
                
                /*
                 * color
                 */ 
                $color = $rPr['color'];
                if($this->detecttype($color)){
                    if($color != ''){
                        $color = '000000';
                    }
                    else{
                        $color = $this->convert2Array($color);
                        $color = $color['@attributes']['val'];
                    }
                    
                    $paraMetaInfo->fontColor = '#'.$color;
                }
                
                /*
                 * font family
                 */
                $fonttype = $rPr['rFonts'];
                if($this->detecttype($fonttype)){
                    $fonttype = $this->convert2Array($fonttype);
                    $paraMetaInfo->fontFamily = $fonttype['@attributes']['ascii'];
                }
                
            }
            
        }
    }
    
    /*
     * extract words from a paragrpah and also the styles of the words
     */
    public function extractWordsandProperties($DPDocument, $ele){
        $ele = $this->convert2Array($ele);
        $r = $ele['r'];
        $rtype = $this->detecttype($r);
        
        $pageCounter = ($DPDocument->body->content->pageCounter) - 1;
        
        $paraCounter = ($DPDocument->body->content->pages[$pageCounter]->contentCounter) - 1;

        $paraMetaInfo = $DPDocument->body->content->pages[$pageCounter]->elements[$paraCounter]->metaInfo;
        
        $words = $DPDocument->body->content->pages[$pageCounter]->elements[$paraCounter]->content;
        
        /*
         * if there are no words in the paragraph
         */
        if($rtype){
            /*
             * if there is only one word
             */
            if ($rtype == 1) {
                $words->addNode();
                $r = $this->convert2Array($r);
                
                /*
                 * check if properties exist
                 */
                
                if($this->detecttype($r['rPr'])){
                    $this->extractWordProperties($words, $r['rPr']);
                }
                
                /*
                 * check if content exists
                 */
                if($this->detecttype($r['t'])){
                    $words->word[$words->wordCounter - 1]->metaInfo->type = 'text';
                    
                    if($r['t'] == ''){
                        $tval = '&nbsp;';
                    }
                    else{
                        $tval = str_replace(' ', '&nbsp;', $r['t']);
                    }
                    $words->word[$words->wordCounter - 1]->content->text = $tval;
                }
                
                /*
                 * else check if it is a drawing
                 */
                else if($this->detecttype($r['drawing'])){
                    $img = $r['drawing']->children($this->docNameSpaces['w']);
                    print_r($img);
                }
            }

            /*
             * if there are more than one word
             */
            elseif ($rtype == 2) {
                foreach($r as $word){
                    $words->addNode();
                    $word = $this->convert2Array($word);
                    if($this->detecttype($word['rPr'])){
                        $this->extractWordProperties($words, $word['rPr']);
                    }
                    
                    /*
                     * check if content exists
                     */
                    if($this->detecttype($word['t'])){
                        $words->word[$words->wordCounter - 1]->metaInfo->type = 'text';
                        
                        if($word['t'] == ''){
                            $wordval = '&nbsp;';
                        }
                        else{
                            $wordval = str_replace(' ', '&nbsp;', $word['t']);
                        }
                        $words->word[$words->wordCounter - 1]->content->text = $wordval;
                    }
                    
                    /*
                     * else check if it is a drawing
                     */
                    else if($this->detecttype($word['drawing'])){
                        $words->word[$words->wordCounter - 1]->metaInfo->type = 'image';
                        
                        $this->extractImages($words, $word['drawing'], $DPDocument);
                    }
                }
            }
        }
        else{
            if($this->listIdentifier){
                $paraMetaInfo->type = 'list';
            }
            else{
                $paraMetaInfo->type = 'blankline';
            }
                
        }
        
    }
    
    /*
     * extract images and info
     */
    public function extractImages($words, $drawing, $DPDocument){
        $words->word[$words->wordCounter - 1]->metaInfo->initImgMetaInfo();
        
        $drawing = $drawing->children($this->docNameSpaces['wp']);
        
        $type = $drawing->getName();
        
        if($type == 'inline'){
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->type = 'inline';
            $imgCounter = $DPDocument->body->content->imgCounter;
            
            
            $a = $drawing->inline;
            $pic = $this->extractImgLink($a);
            
            /*
             * width and height
             */
            $extent = $a->extent;
            $extent = $this->convert2Array($extent->attributes());

            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->width = $extent['@attributes']['cx'];
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->height = $extent['@attributes']['cy'];
            
            
            $words->word[$words->wordCounter - 1]->content->imgLink = $pic;
            $DPDocument->body->content->imgCounter++;
        }
        else if($type == 'anchor'){
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->type = 'anchor';
            $imgCounter = $DPDocument->body->content->imgCounter;
            
            $a = $drawing->anchor;
            $pic = $this->extractImgLink($a);
            
            /*
             * width and height
             */
            $extent = $a->extent;
            $extent = $this->convert2Array($extent->attributes());

            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->width = $extent['@attributes']['cx'];
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->height = $extent['@attributes']['cy'];
            
            
            
            $anchorattr = $this->convert2Array($a->attributes());
            
            /*
             * behind doc
             */
            
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->behindDoc = $anchorattr['@attributes']['behindDoc'];
            
            /*
             * Z-index
             */
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->zIndex = $anchorattr['@attributes']['realtiveHeight'];
            
            /*
             * image margins
             */
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->marginTop = $anchorattr['@attributes']['distT'];
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->marginBottom = $anchorattr['@attributes']['distB'];
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->marginLeft = $anchorattr['@attributes']['distL'];
            $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->marginRight = $anchorattr['@attributes']['distR'];
            
            /*
             * simple pos
             */
            if($anchorattr['@attributes']['simplePos']){
                $cor = $a->simplePos;
                $cor = $this->convert2Array($cor->attributes());
                
                $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->referenceX = 'pageLeft';
                $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->referenceY = 'pageTop';
                $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->posX = $cor['@attributes']['x'];
                $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->posY = $cor['@attributes']['y'];
            }
            else{
                $posx = $a->positionH;
                $posy = $a->positionV;
                
                $posxattr = $this->convert2Array($posx->attributes());
                $posyattr = $this->convert2Array($posy->attributes());
                
                $posxalign = '';
                $posyalign = '';
                
                if($posx->align){
                    $posxalign = (string)$posx->align;
                }
                else{
                    $posxalign = (string)$posx->posOffset;
                }
                
                if($posy->align){
                    $posyalign = (string)$posy->align;
                }
                else{
                    $posyalign = (string)$posy->posOffset;
                }
                
                $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->referenceX = $posxattr['@attributes']['relativeFrom'];
                $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->referenceY = $posyattr['@attributes']['relativeFrom'];
                $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->posX = $posxalign;
                $words->word[$words->wordCounter - 1]->metaInfo->imageMetaInfo->posY = $posyalign;
            }
            
            $words->word[$words->wordCounter - 1]->content->imgLink = $pic;
            $DPDocument->body->content->imgCounter++;
        }
            
    }

    public function extractImgLink($a){
        $a = $a->children($this->docNameSpaces['a']);
        $a = $a->graphic->graphicData;
        $pic = $a->children($this->docNameSpaces['pic']);
        $pic = $pic->pic->blipFill;
        $pic = $pic->children($this->docNameSpaces['a']);
        $pic = $pic->blip;
        $pic = $this->convert2Array($pic->attributes($this->docNameSpaces['r']));
        $pic = $pic['@attributes']['embed'];
        $pic = $this->wordRelsInfo[$pic];
        return $pic['Target'];
    }
    
    /*
     * extracts word properties and styles
     */
    public function extractWordProperties($words, $rPr){
        $rPr = $this->convert2Array($rPr);
        
        /*
         * bold 
         */
        if($this->detecttype($rPr['b'])){
            $words->word[$words->wordCounter - 1]->metaInfo->bold = 1;
        }
        
        /*
         * italics
         */
        if($this->detecttype($rPr['i'])){
            $words->word[$words->wordCounter - 1]->metaInfo->italics = 1;
        }
        
        /*
         * font size
         */
        $sz = $this->convert2Array($rPr['sz']);
        if($this->detecttype($sz)){
            $words->word[$words->wordCounter - 1]->metaInfo->fontSize = $sz['@attributes']['val']/2;
        }
        
        /*
         * color
         */ 
        $color = $rPr['color'];
        if($this->detecttype($color)){
            if($color != ''){
                $color = '000000';
            }
            else{
                $color = $this->convert2Array($color);
                $color = $color['@attributes']['val'];
            }

            $words->word[$words->wordCounter - 1]->metaInfo->fontColor = '#'.$color;
        }
        
        /*
         * font family
         */
        $fonttype = $rPr['rFonts'];
        if($this->detecttype($fonttype)){
            $fonttype = $this->convert2Array($fonttype);
            $words->word[$words->wordCounter - 1]->metaInfo->fontFamily = $fonttype['@attributes']['ascii'];
        }
    }
    
    /*
     * extracts table information
     */
    public function extractTableData($DPDocument, $ele){
        $tbldata = $ele->tr;
        
        foreach ($tbldata as $tr){
            
        }
        
    }
    
    /*
     * extracts table content
     */
    
    /*
            converts docx to DP Format and returns datastructure
    */
    public function publishDPFormat(){
            $this->extractFile();
            $targetarr = $this->getSchemasTarget($this->relsInfo);
            $fp = $this->mainDocument();
            $this->getDOCXNameSpaces($fp); // sets Docx namespaces
            $this->registerCustomNameSpaces(); // sets custom namespaces
            $body = $fp->children($this->docNameSpaces['w']);
            
            /*
             * Initialize the DocsPad data structure
             */
            $DPDocument = new DPDocument('DOCX');
            
            /*
             * word background properties
             * document level
             */
            $this->documentBackground($DPDocument, $body);
            
            /*
             * document defaults and settings
             * document level
             */
            $this->documentPageProperties($DPDocument, $body);
            
            /*
             * paragraphProperties
             */
            $this->documentParagraphDefaults($DPDocument, $body);
            
            /*
             * add Page
             */
            $DPDocument->body->content->addPage();
            
            /*
             * body extraction 
             */
            foreach($body->body->children($this->docNameSpaces['w']) as $ele){
                $name = $ele->getName();
                if($name == 'p'){
                    /*
                     * documents paragraph properties
                     */
                    $this->extractParagraphProperties($DPDocument, $ele);
                    
                    /*
                     * documents word extraction
                     */
                    $this->extractWordsandProperties($DPDocument, $ele);
                    
                }
                else if($name == 'tbl'){
                    $this->extractTableData($DPDocument, $ele);
                }
            }
            
            print_r($DPDocument);


//            echo "<pre>";
//            print_r($DPDocument);
//            echo "</pre>";

//            foreach($body->body->p as $para){
//                    $para = $this->returnArray($para);
//
//                    $this->contentAddNode($DPDocument);
//
//                    /* !para meta info extract */
//                    $this->paraMetaInfo($DPDocument, $para, $contentCounter);
//
//                    $counter['words'] = 0;
//                    $rtype = $this->detecttype($para['r']);
//                    $r = $this->returnArray($para['r']);
//
//                    if($rtype == 1){
//                            $this->wordAddNode($DPDocument, $contentCounter);
//
//                            /*
//                             * word meta info
//                             */
//                            $this->wordMetaInfo($DPDocument, $r, $contentCounter);
//
//                            /* !word */
//                            $wtype = $this->detecttype($r['t']);
//                            if($wtype == 1){
//                                    $word = $this->returnArray($word);
//
//                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->type = 'text';
//                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->content->text = $word[0];
//                            }
//                            else if($wtype == 3){
//                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->type = 'text';
//                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->content->text = $r['t'];
//                            }
//                            else if($wtype == 0){
//                                    //print_r($word);
//                                    $drawingtype = $this->detecttype($word['drawing']);
//                                    if($drawingtype == 1){
//                                            $counter['images']++;
//
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->type = 'image';
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->content->link = $this->extractToDir.'word/media/image'.$counter['images'];
//
//                                            $img = $word['drawing'];
//                                            $imgs = $img->children($this->docNameSpaces['wp']);
//                                            $imgs = $this->returnArray($imgs);
//                                            $inline = $imgs['inline'];
//                                            $extent = $inline->extent;
//                                            $extent = $this->returnArray($extent->attributes());
//
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->imageWidth = $this->EMU2pixel($extent['@attributes']['cx']).'px';
//                                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->imageHeight = $this->EMU2pixel($extent['@attributes']['cy']).'px';
//
//                                    }
//                            }
//                    }
//                    else if($rtype == 2){				
//                            foreach($r as $wordno=>$word){
//                                    $this->wordAddNode($DPDocument, $contentCounter);
//                                    $word = get_object_vars($word);
//                                    $rPr = $this->returnArray($word['rPr']);
//
//
//                                    $wrdcnt = $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content;
//
//                                    /* !font Family */
//                                    if($this->detecttype($rPr['rFonts']) != 0){
//                                            $fonttype = $this->returnArray($rPr['rFonts']);
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->fontFamily = $fonttype['@attributes']['ascii'];
//                                    }
//
//                                    /* !bold detect */
//                                    if($this->detecttype($rPr['b']) != 0){
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->bold = 1;
//
//                                    }
//
//                                    /* !italics detect */
//                                    if($this->detecttype($rPr['i']) != 0){
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->italics = 1;
//                                    }
//
//                                    /* !underline detect */
//                                    if($this->detecttype($rPr['u']) != 0){
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->underline = 1;
//                                    }
//
//                                    /* !fontcolor */
//                                    if($this->detecttype($rPr['color']) != 0){
//                                            $color = $this->returnArray($rPr['color']);
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->fontColor = '#'.$color['@attributes']['val'];
//                                    }
//
//                                    /* !fontsize */
//                                    if($this->detecttype($rPr['sz']) != 0){
//                                            $size = $this->returnArray($rPr['sz']);
//
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->fontSize = ($size['@attributes']['val']/2).'pt';
//                                    }
//
//                                    /* !word */
//                                    $wtype = $this->detecttype($word['t']);
//                                    if($wtype == 1){
//                                            $word = $this->returnArray($word['t']);
//
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->type = 'text';
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->content->text = $word[0];
//                                    }
//                                    else if($wtype == 3){
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->type = 'text';
//                                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->content->text = $word['t'];
//
//                                    }
//                                    else if($wtype == 0){
//                                            /* !images */
//                                            $drawingtype = $this->detecttype($word['drawing']);
//                                            if($drawingtype == 1){
//                                                    $counter['images']++;
//
//                                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->type = 'image';
//                                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->content->link = $this->extractToDir.'word/media/image'.$counter['images'];
//
//                                                    $img = $word['drawing'];
//                                                    $imgs = $img->children($this->docNameSpaces['wp']);
//                                                    $imgs = $this->returnArray($imgs);
//                                                    $inline = $imgs['inline'];
//                                                    $extent = $inline->extent;
//                                                    $extent = $this->returnArray($extent->attributes());
//
//                                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->imageWidth = $this->EMU2pixel($extent['@attributes']['cx']).'px';
//                                                    $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->content->word[$wrdcnt->wordCounter - 1]->metaInfo->imageHeight = $this->EMU2pixel($extent['@attributes']['cy']).'px';
//
//                                            }
//                                    }
//
//                                    $counter['words']++;
//                            }
//                    }
//                    else if($rtype == 0){
//                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->metaInfo->type = 'emptyline';
//                            $rPr = $this->returnArray($pPr['rPr']);
//                            $sz = $this-> returnArray($rPr['sz']);
//                            $sz = $sz['@attributes']['val'];
//                            $DPDocument->body->content->elements[$contentCounter->contentCounter - 1]->metaInfo->fontSize = ($sz/2).'pt';
//                    }
//
//
//
//                    $counter['main']++;
//                    //foreach()
//            }
//            //print_r($DPformat);
//            $this->dataStructure = $DPDocument;
//
//            return $DPDocument;
            //print_r($body);
    }

    public function saveDataStructure(){
            $ourFileHandle = fopen($this->convertedFile.'DPStructure.bkpd', 'w') or die("can't open file");
            fclose($ourFileHandle);
            return file_put_contents($this->convertedFile.'DPStructure.bkpd', print_r( serialize($this->dataStructure) , true));
    }

    public function getparatype($para){
            $pPr = $para['pPr'];
            $r =	 $para['r'];
            $attr = $para['@attibutes'];

            if($this->detecttype($pPr)){
                    //
            }
    }
    
    /*
     * detect the type of the input variable
     * object - 1
     * array - 2
     * string - 3
     * NULL - 0
     */
    public function detecttype($ele){
            switch(gettype($ele)){
                    case 'object':
                            return 1;
                            break;
                    case 'array':
                            if(sizeof($ele) == 0){
                                return 0;
                            }
                            else{
                                return 2;
                            }
                            break;
                    case 'string':
                            return 3;
                            break;
                    case 'NULL':
                            return 0;
                            break;
            }
    }
    
    /*
     * covnerts to any thing into an array
     */
    public function convert2Array($ele){
        switch($this->detecttype($ele)){
            case 0:
                return 0;
                break;
            
            case 1:
                return get_object_vars($ele);
                break;
            
            case 2:
                return $ele;
                break;
            
            default: throw new Exception('Invalid Input Type to convert to array');
        }
    }
    
    /*
     * deletes folders with files and folders recursively usinf directory iterator
     * note: needs php 5.2+
     */
    public function deleteFolder($dir){
        $it = new RecursiveDirectoryIterator($dir);
        $files = new RecursiveIteratorIterator($it,
                     RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->getFilename() === '.' || $file->getFilename() === '..') {
                continue;
            }
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);
    }
}


?>