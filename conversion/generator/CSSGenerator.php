<?php

class CSSGenerator{
    public $style;
    
    public function setInlineStyle(){
        $this->style = '';
    }
    
    public function setInlineStyleAttr($key, $val){
        $this->style = $this->style.$key.':'.$val;
    }
    
    public function getInlineStyle(){
        return $this->style;
    }
}

?>
