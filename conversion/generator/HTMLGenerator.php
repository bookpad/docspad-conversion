<?php

class HTMLGenerator{
    private $startTag;
    private $content;
    private $endTag;
    
    public function __construct($tagName) {
        $this->startTag = $tagName;
        $this->endTag = '</'.$tagName.'>';
        $this->content = '';
    }
    
    public function setContent($content){
        $this->content = $this->content.$content;
    }
    
    public function setAttribute($name, $val){
        $this->startTag = $this->startTag.' '.$name.'="'.$val.'"';
    }
    
    public function getTag(){
        return '<'.$this->startTag.'>'.$this->content.$this->endTag;
    }
    
}

?>
