<html>
    <body>
<?php

include_once 'DPConvertDOCX2.php';
include_once 'DPHTMLPublish.php';

try{

$inst = new DPConvertDOCX('lve1.docx','','testextracteddocx/','testextractedhtml/');
}
catch(Exception $e){
    echo $e->getMessage();
}

$inst->publishDPFormat();

$data = file_get_contents(($inst->convertedFile).'DPStructure.bkpd');

$inst1 = new DPHTMLPublish($data);
$inst1->DOCXPublishHTML();

?>

        <script src="docspad/jquery.js"></script>
        <script src="docspad/docx.js"></script>
        <script>

                    $(document).ready(function() {
                        var ref = $('div.DocPage').docsPadReader();
                        ref = ref.data('plugin_docsPadReader');
                        ref.renderPages();
                    });


        </script>
    </body>
</html>