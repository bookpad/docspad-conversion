<?php

/* !dependencies */
include_once('entities/DPDocument.php');

class DPConvertPPT{
    public $fileName; // docx file name
    public $filePath; // file path relative to the location of the class file
    public $extractToDir; // file extraction directory	
    public $convertedFile; // converted data structure
    public $dataStructure; // complete converted data into a organised structure
    public $docNameSpaces;
    public $targetArr;
    private $mainDocument; // path to main document
    private $mainDocumentDir; // directory for main document

    public function __construct($fileName, $filePath, $extractToDir, $convertedFile) {
        $this->fileName = $fileName;
        $this->filePath = $filePath;
        $this->extractToDir = $extractToDir;
        $this->convertedFile = $convertedFile;
        if(is_dir($this->convertedFile)){
            $this->deleteFolder($this->convertedFile);
        }
        mkdir($this->convertedFile);
    }
    
    /*
     * extracts the docx file to the mentioned directory
     */
    public function extractFile() {
            $zip = new ZipArchive();
            $res = $zip->open(($this->filePath).($this->fileName));

            if($res === TRUE){
                    $zip->extractTo($this->extractToDir);
                    $zip->close();
                    return 1;
            }
            else{
                    return 0;
            }
    }
    
    /*
     * publish your datastructure here
     */
    public function publishDPFormat(){
        $DPDocument = new DPDocument('PPT');
        
        //adding page equivalent is slide for you
        $DPDocument->body->content->addPage();
        
        //next page counter meaning as you add page counter increases by one so all operations will be on pgcounter - 1
        $pgcounter = $DPDocument->body->content->pageCounter;

        //add paragraph Node
        $DPDocument->body->content->pages[$pgcounter - 1]->addNode();
        $concounter = $DPDocument->body->content->pages[$pgcounter - 1]->contentCounter;
        
        //add Word Node
        $DPDocument->body->content->pages[$pgcounter - 1]->elements[$concounter - 1]->content->addNode();
        
        //as you see the datastructure you ll understand everything i guess
        echo "<pre>";
        print_r($DPDocument);
        echo "</pre>";
    }
    
    /*
     * detect the type of the input variable
     * object - 1
     * array - 2
     * string - 3
     * NULL - 4
     */
    public function detecttype($ele){
            switch(gettype($ele)){
                    case 'object':
                            return 1;
                            break;
                    case 'array':
                            return 2;
                            break;
                    case 'string':
                            return 3;
                            break;
                    case 'NULL':
                            return 0;
                            break;
            }
    }
    
    /*
     * covnerts to any thing into an array
     */
    public function convert2Array($ele){
        switch($this->detecttype($ele)){
            case 0:
                return 0;
                break;
            
            case 1:
                return get_object_vars($ele);
                break;
            
            case 2:
                return $ele;
                break;
            
            default: throw new Exception('Invalid Input Type to convert to array');
        }
    }
    
    /*
     * deletes folders with files and folders recursively usinf directory iterator
     * note: needs php 5.2+
     */
    public function deleteFolder($dir){
        $it = new RecursiveDirectoryIterator($dir);
        $files = new RecursiveIteratorIterator($it,
                     RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->getFilename() === '.' || $file->getFilename() === '..') {
                continue;
            }
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);
    }
}


//sample code

try{

$inst = new DPConvertPPT('lve1.docx','','testextracteddocx/','testextractedhtml/');
}
catch(Exception $e){
    echo $e->getMessage();
}

$data = $inst->publishDPFormat();
    
?>
