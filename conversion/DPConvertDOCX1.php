<?php

class DPConvertDOCX{
	public $fileName; // docx file name
	public $filePath; // file path relative to the location of the class file
	public $extractToDir; // file extraction directory	
	public $convertedHTML; // published HTML
	private $mainDocument; // path to main document
	private $mainDocumentDir; // directory for main document
	public $dataStructure; // complete converted data into a organised structure
	public $docNameSpaces;
	
	public function __construct($fileName, $filePath, $extractToDir, $convertedHTML) {
		$this->fileName = $fileName;
		$this->filePath = $filePath;
		$this->extractToDir = $extractToDir;
		$this->convertedHTML = $convertedHTML;
	}
	
	/*
		!pixel to emu
	*/
	public function EMU2pixel($EMU){
		return $EMU/9525;
	}
	
	/*
		extracts the docx file to the mentioned directory
	*/
	public function extractFile() {
		$zip = new ZipArchive();
		$res = $zip->open(($this->filePath).($this->fileName));
		
		if($res === TRUE){
			$zip->extractTo($this->extractToDir);
			$zip->close();
			return 1;
		}
		else{
			return 0;
		}
	}
	
	/*
		extracts schemas, IDs and targets
	*/
	public function getRelsInfo() {
		$rels = simplexml_load_file(($this->extractToDir).'_rels/.rels');
		if($rels){
			$data = array();
			$relationships = $rels->Relationship;
			foreach($relationships as $relationship){
				$val = get_object_vars($relationship);
				array_push($data, $val['@attributes']);
			}
			return $data;
		}
		else{
			return 0;
		}
	}
	
	/*
		get different schemas
	*/
	public function getSchemasTarget($relsarr) {
		$data = array();
		foreach($relsarr as $rel){
			$temp = $rel['Type'];
			$temparr = preg_split('/\//', $temp);
			$data[$temparr[sizeof($temparr) - 1]] = $rel['Target'];
		}
		return $data;
	}
	
	/*
		extract text
	*/
	public function mainDocument($targetArr) {
		$fpxml = simplexml_load_file(($this->extractToDir).$targetArr['officeDocument']);
		return $fpxml;
	}
	
	/*
		extract all doc namespaces
	*/
	public function getDOCXNameSpaces($xmlhandle){
		$this->docNameSpaces = $xmlhandle->getDocNamespaces();
	}
	
	/*
		extract fontTable
	*/
	public function wordRelInfo($targetArr) {
		$str = $targetArr['officeDocument'];
		$str = preg_split('/\//', $str);
		$this->mainDocumentDir = $str[0];
		
		$fpxml = simplexml_load_file(($this->extractToDir).$str[0].'/_rels/document.xml.rels');
		$data = array();
		$relationships = $fpxml->Relationship;
		foreach($relationships as $relationship){
			$val = get_object_vars($relationship);
			$temp = $val['@attributes'];
			$type = $temp['Type'];
			$temparr = preg_split('/\//', $type);
			$data[$temparr[sizeof($temparr) - 1]] = $temp['Target'];
		}
		return $data;
	}
	
	
	/*
		converts docx to DP Format and returns datastructure
	*/
	public function publishDPFormat(){
		$DPformat = array();
		$this->extractFile();
		$relsarr = $this->getRelsInfo();
		$targetarr = $this->getSchemasTarget($relsarr);
		$fp = $this->mainDocument($targetarr);
		$this->getDOCXNameSpaces($fp); // !sets Docx namespaces
			
		$body = $fp->children($this->docNameSpaces['w']);
		
		/* !format main skeleton declaration */
		$DPformat['document'] = array();
		$DPformat['document']['metainfo'] = array();
		
		/* !document default properties */
		$DPformat['document']['defaults'] = array();
		$DPformat['document']['defaults']['fontsize'] = 12;
		$DPformat['document']['defaults']['fontcolor'] = '#000000';
		
		$DPformat['document']['body'] = array();
		$DPformat['document']['body']['metainfo'] = array();
		
		/* !body metainfo */
		$background = $this->returnArray($body->background);
		$DPformat['document']['body']['metainfo']['backgroundcolor'] = $background['@attributes']['color'];
		
		$DPformat['document']['body']['content'] = array();
		
		/* !counter declarations */
		$counter = array();
		$counter['main'] = 0;
		$counter['images'] = 0;
		
		foreach($body->body->p as $parano=>$para){
			$para = $this->returnArray($para);
			
			$DPformat['document']['body']['content'][$counter['main']] = array();
			$DPformat['document']['body']['content'][$counter['main']]['metainfo'] = array();
			
			/* !para meta info extract */
			$pPr = $this->returnArray($para['pPr']);
			if($pPr != 0){
				
				/* !para alignment */
				$jc = $this->returnArray($pPr['jc']);
				if($jc)
					$DPformat['document']['body']['content'][$counter['main']]['metainfo']['textalign'] = $jc['@attributes']['val'];
				
			}
										
			$counter['words'] = 0;
			$rtype = $this->detecttype($para['r']);
			$r = $this->returnArray($para['r']);
			
			if($rtype == 1){
				$DPformat['document']['body']['content'][$counter['main']]['content'] = array();
				$rPr = $this->returnArray($r['rPr']);
				$word = $r['t'];
				$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo'] = array();
				
				/* !bold detect */
				if($this->detecttype($rPr['b']) != 0){
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['bold'] = 1;
				}
				
				/* !italics detect */
				if($this->detecttype($rPr['i']) != 0){
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['italics'] = 1;
				}
				
				/* !underline detect */
				if($this->detecttype($rPr['u']) != 0){
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['underline'] = 1;
				}
				
				/* !fontcolor */
				if($this->detecttype($rPr['color']) != 0){
					$color = $this->returnArray($rPr['color']);
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['fontcolor'] = '#'.$color['@attributes']['val'];
				}
				
				/* !fontsize */
				if($this->detecttype($rPr['sz']) != 0){
					$size = $this->returnArray($rPr['sz']);
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['fontsize'] = '#'.$size['@attributes']['val'];
				}
				
				/* !word */
				$wtype = $this->detecttype($r['t']);
				if($wtype == 1){
					$word = $this->returnArray($word);
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['type'] = 'text';
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['content']['text'] = $word[0];
				}
				else if($wtype == 3){
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['type'] = 'text';
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['content']['text'] = $r['t'];
				}
				else if($wtype == 0){
					$drawingtype = $this->detecttype($word['drawing']);
					if($drawingtype == 1){
						$counter['images']++;
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['type'] = 'image';
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['content']['link'] = $counter['images'];
						$img = $word['drawing'];
						$imgs = $img->children($this->docNameSpaces['wp']);
						$imgs = $this->returnArray($imgs);
						$inline = $imgs['inline'];
						$extent = $inline->extent;
						$extent = $this->returnArray($extent->attributes());
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['width'] = $this->EMU2pixel($extent['@attributes']['cx']).'px';
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['height'] = $this->EMU2pixel($extent['@attributes']['cy']).'px';
					}
				}
			}
			else if($rtype == 2){
				$DPformat['document']['body']['content'][$counter['main']]['content'] = array();
				foreach($r as $wordno=>$word){
					$word = get_object_vars($word);
					$rPr = $this->returnArray($word['rPr']);
					$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo'] = array();
					
					/* !bold detect */
					if($this->detecttype($rPr['b']) != 0){
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['bold'] = 1;
					}
					
					/* !italics detect */
					if($this->detecttype($rPr['i']) != 0){
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['italics'] = 1;
					}
					
					/* !underline detect */
					if($this->detecttype($rPr['u']) != 0){
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['underline'] = 1;
					}
					
					/* !fontcolor */
					if($this->detecttype($rPr['color']) != 0){
						$color = $this->returnArray($rPr['color']);
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['fontcolor'] = '#'.$color['@attributes']['val'];
					}
					
					/* !fontsize */
					if($this->detecttype($rPr['sz']) != 0){
						$size = $this->returnArray($rPr['sz']);
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['fontsize'] = '#'.$size['@attributes']['val'];
					}
					
					/* !word */
					$wtype = $this->detecttype($word['t']);
					if($wtype == 1){
						$word = $this->returnArray($word['t']);
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['type'] = 'text'; 
 						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['content']['text'] = $word[0];
					}
					else if($wtype == 3){
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['type'] = 'text'; 
						$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['content']['text'] = $word['t'];
					}
					else if($wtype == 0){
						/* !images */
						$drawingtype = $this->detecttype($word['drawing']);
						if($drawingtype == 1){
							$counter['images']++;
							$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['type'] = 'image';
							$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['content']['link'] = $counter['images'];
							$img = $word['drawing'];
							$imgs = $img->children($this->docNameSpaces['wp']);
							$imgs = $this->returnArray($imgs);
							$inline = $imgs['inline'];
							$extent = $inline->extent;
							$extent = $this->returnArray($extent->attributes());
							$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['width'] = $this->EMU2pixel($extent['@attributes']['cx']).'px';
							$DPformat['document']['body']['content'][$counter['main']]['content'][$counter['words']]['metainfo']['height'] = $this->EMU2pixel($extent['@attributes']['cy']).'px';
						}
					}
					
					$counter['words']++;
				}
			}
			else if($rtype == 0){
				$DPformat['document']['body']['content'][$counter['main']]['metainfo']['type'] = 'emptyline';
			}
			
			
			
			$counter['main']++;
			//foreach()
		}
		
		//print_r($DPformat);
		return $DPformat;
		//print_r($body);
	}
	
	public function getparatype($para){
		$pPr = $para['pPr'];
		$r = $para['r'];
		$attr = $para['@attibutes'];
		
		if($this->detecttype($pPr)){
			//
		}
	}
	
	public function returnArray($ele){
		switch($this->detecttype($ele)){
			case 0:
				return 0;
				break;
			case 1:
				return get_object_vars($ele);
			case 2:
				return $ele;
		}
	}
	
	public function detecttype($ele){
		switch(gettype($ele)){
			case 'object':
				return 1;
				break;
			case 'array':
				return 2;
				break;
			case 'string':
				return 3;
				break;
			case 'NULL':
				return 0;
				break;
		}
	}
}


?>