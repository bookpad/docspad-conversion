<?php

class DPConvertDOCX {
	public $fileName; // docx file name
	public $filePath; // file path relative to the location of the class file
	public $extractToDir; // file extraction directory	
	public $convertedHTML; // published HTML
	private $mainDocument; // path to main document
	private $mainDocumentDir; // directory for main document
	public $dataStructure; // complete converted data into a organised structure
	
	public function __construct($fileName, $filePath, $extractToDir, $convertedHTML) {
		$this->fileName = $fileName;
		$this->filePath = $filePath;
		$this->extractToDir = $extractToDir;
		$this->convertedHTML = $convertedHTML;
	}
	
	/*
		extracts the docx file to the mentioned directory
	*/
	public function extractFile() {
		$zip = new ZipArchive();
		$res = $zip->open(($this->filePath).($this->fileName));
		
		if($res === TRUE){
			$zip->extractTo($this->extractToDir);
			$zip->close();
			return 1;
		}
		else{
			return 0;
		}
	}
	
	/*
		extracts schemas, IDs and targets
	*/
	public function getRelsInfo() {
		$rels = simplexml_load_file(($this->extractToDir).'_rels/.rels');
		if($rels){
			$data = array();
			$relationships = $rels->Relationship;
			foreach($relationships as $relationship){
				$val = get_object_vars($relationship);
				array_push($data, $val['@attributes']);
			}
			return $data;
		}
		else{
			return 0;
		}
	}
	
	/*
		get different schemas
	*/
	public function getSchemasTarget($relsarr) {
		$data = array();
		foreach($relsarr as $rel){
			$temp = $rel['Type'];
			$temparr = preg_split('/\//', $temp);
			$data[$temparr[sizeof($temparr) - 1]] = $rel['Target'];
		}
		return $data;
	}
	
	/*
		extract text
	*/
	public function mainDocument($targetArr) {
		$fpxml = simplexml_load_file(($this->extractToDir).$targetArr['officeDocument']);
		$fpxml->registerXPathNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
		return $fpxml;
	}
	
	/*
		extract fontTable
	*/
	public function wordRelInfo($targetArr) {
		$str = $targetArr['officeDocument'];
		$str = preg_split('/\//', $str);
		$this->mainDocumentDir = $str[0];
		
		$fpxml = simplexml_load_file(($this->extractToDir).$str[0].'/_rels/document.xml.rels');
		$data = array();
		$relationships = $fpxml->Relationship;
		foreach($relationships as $relationship){
			$val = get_object_vars($relationship);
			$temp = $val['@attributes'];
			$type = $temp['Type'];
			$temparr = preg_split('/\//', $type);
			$data[$temparr[sizeof($temparr) - 1]] = $temp['Target'];
		}
		return $data;
	}
	
	public function buildDataStructure(){
		$this->extractFile();

		$tar = $this->getRelsInfo();
		
		$tararr = $this->getSchemasTarget($tar);
		
		$fpxml = $this->mainDocument($tararr);
		
		$tempdataStructure = array();
		
		$tempdataStructure['body'] = array();
		
		$paras = $fpxml->xpath('//w:p');
		$tempdataStructure['body'] = array();
		
		/* count array */
		$count = array();
		$count['paras'] = 0;
		$count['listitem'] = 0;
		$count['maindata'] = -1;
		
		/* flag array */
		$flags = array();
		$flags['listsprev'] = '';
		
		foreach($paras as $para){
				//print_r($para->children('http://schemas.openxmlformats.org/wordprocessingml/2006/main'));
				$para_attr = $para->attributes('http://schemas.openxmlformats.org/wordprocessingml/2006/main');
				$children = $para->children('http://schemas.openxmlformats.org/wordprocessingml/2006/main');
				$children = get_object_vars($children);
				
				$ptype = $this->identifyParaType($children['pPr'], $children['r']);
				$palign = $children['pPr']->jc;
				$palign = (sizeof($palign) == 0)?'left':get_object_vars($palign);
				$palign = (gettype($palign) == 'array')?$palign['@attributes']['val']:$palign;
				
				switch($ptype){
					/* blank line */
					case 0:
						$count['maindata']++;
						$tempdataStructure['body'][$count['maindata']]['metainfo'] = array();
						$tempdataStructure['body'][$count['maindata']]['metainfo']['type'] = 'blankline';
					break;
					
					/* plain text paragraph */
					case 1:
						$count['maindata']++;
						$count['words'] = 0;
						$tempdataStructure['body'][$count['maindata']]['metainfo'] = array();
						$tempdataStructure['body'][$count['maindata']]['metainfo']['type'] = 'paragraph';
						$tempdataStructure['body'][$count['maindata']]['metainfo']['align'] = $palign;
						$tempdataStructure['body'][$count['maindata']]['metainfo']['parano'] = $count['paras'];
						$tempdataStructure['body'][$count['maindata']]['content'] = array();
						
						//print_r($children);
						//echo $para_count;
						if(sizeof($children['r']) >= 1){
							foreach($children['r'] as $child){
								$metainfo = get_object_vars($child);
								$color = $metainfo['rPr']->color;
								$fontsize = $metainfo['rPr']->sz;
								$fontfamily = $metainfo['rPr']->rFonts;
								$italics = sizeof($metainfo['rPr']->i);
								$strike = sizeof($metainfo['rPr']->strike);
								$underline = sizeof($metainfo['rPr']->u);
								//print_r($metainfo['t']);
								
								$fontfamily = (sizeof($fontfamily) == 0)?'Cambria':get_object_vars($fontfamily);
								$fontfamily = (gettype($fontfamily) == 'array')?$fontfamily['@attributes']['ascii']:$fontfamily;
								$color = (sizeof($color) == 0)?'000000':get_object_vars($color);
								$color = (gettype($color) == 'array')?$color['@attributes']['val']:$color;
								$fontsize = (sizeof($fontsize) == 0)?'12':get_object_vars($fontsize);
								$fontsize = (gettype($fontsize) == 'array')?$fontsize['@attributes']['val']:$fontsize;
								
								$tempdataStructure['body'][$count['maindata']]['content'][$count['words']]['fontfamily'] = $fontfamily;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['words']]['bold'] = $bold;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['words']]['underline'] = $underline;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['words']]['italics'] = $italics;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['words']]['strike'] = $strike;
								
								$tempdataStructure['body'][$count['maindata']]['content'][$count['words']]['color'] = $color;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['words']]['fontsize'] = $fontsize;
								$word = '';
								if(gettype($metainfo['t']) == 'object'){
									$word = get_object_vars($metainfo['t']);
									$word = $word[0];
								}
								else{
									$word = $metainfo['t'];
								}
								$tempdataStructure['body'][$count['maindata']]['content'][$count['words']]['word'] = $word;
								$count['words']++;
							}
						}
						else{
							//print_r($children['r']);
							$tempdataStructure['body'][$count['maindata']]['paras'][$para_count]['words'][0]['word'] = '';
						}
						$count['paras']++;
					break;
					
					/* if list item has more than one word */
					case 2:
						if(strcmp($flags['listsprev'],$para_attr['rsidRDefault']) == 0){
							$flags['lists'] = 1;
							$count['listitem']++;
						}
						else{
							$count['maindata']++;
							$tempdataStructure['body'][$count['maindata']]['metainfo'] = array();
							$tempdataStructure['body'][$count['maindata']]['metainfo']['type'] = 'list';
							$tempdataStructure['body'][$count['maindata']]['metainfo']['listtype'] = 'underprogress';
							$tempdataStructure['body'][$count['maindata']]['content'] = array();
							$flags['listsprev'] = $para_attr['rsidRDefault'];
							$count['listitem'] = 0;
							$flags['lists'] = 0;
						}
						
						$count['words'] = 0;
						
						if(sizeof($children['r']) >= 1){
							if(gettype($children['r']) == 'object'){
								//print_r($children['r']);
								$metainfo = $children['r'];
								$metainfo = get_object_vars($metainfo);
								//echo $metainfo['t'];
								$color = $metainfo['rPr']->color;
								$fontsize = $metainfo['rPr']->sz;
								$fontfamily = $metainfo['rPr']->rFonts;
								$italics = sizeof($metainfo['rPr']->i);
								$strike = sizeof($metainfo['rPr']->strike);
								$underline = sizeof($metainfo['rPr']->u);
								
								$fontfamily = (sizeof($fontfamily) == 0)?'Cambria':get_object_vars($fontfamily);
								$fontfamily = (gettype($fontfamily) == 'array')?$fontfamily['@attributes']['ascii']:$fontfamily;
								$color = (sizeof($color) == 0)?'000000':get_object_vars($color);
								$color = (gettype($color) == 'array')?$color['@attributes']['val']:$color;
								$fontsize = (sizeof($fontsize) == 0)?'12':get_object_vars($fontsize);
								$fontsize = (gettype($fontsize) == 'array')?$fontsize['@attributes']['val']:$fontsize;
								
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['meta']['align'] = $palign;
								
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['fontfamily'] = $fontfamily;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['fontfamily'] = $fontfamily;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['bold'] = $bold;

								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['underline'] = $underline;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['italics'] = $italics;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['strike'] = $strike;
								
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['color'] = $color;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['fontsize'] = $fontsize;
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['word'] = $metainfo['t'];
							}
							else{
								$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['meta']['align'] = $palign;
								foreach($children['r'] as $child){
									$metainfo = get_object_vars($child);
									$color = $metainfo['rPr']->color;
									$fontsize = $metainfo['rPr']->sz;
									$fontfamily = $metainfo['rPr']->rFonts;
									$italics = sizeof($metainfo['rPr']->i);
									$strike = sizeof($metainfo['rPr']->strike);
									$underline = sizeof($metainfo['rPr']->u);
									$bold = sizeof($metainfo['rPr']->b);
									//print_r($metainfo['t']);
									
									$fontfamily = (sizeof($fontfamily) == 0)?'Cambria':get_object_vars($fontfamily);
									$fontfamily = (gettype($fontfamily) == 'array')?$fontfamily['@attributes']['ascii']:$fontfamily;
									$color = (sizeof($color) == 0)?'000000':get_object_vars($color);
									$color = (gettype($color) == 'array')?$color['@attributes']['val']:$color;
									$fontsize = (sizeof($fontsize) == 0)?'12':get_object_vars($fontsize);
									$fontsize = (gettype($fontsize) == 'array')?$fontsize['@attributes']['val']:$fontsize;
									
									
									$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['fontfamily'] = $fontfamily;
									$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['bold'] = $bold;
									$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['underline'] = $underline;
									$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['italics'] = $italics;
									$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['strike'] = $strike;
									$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['color'] = $color;
									$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['fontsize'] = $fontsize;
									$word = '';
									if(gettype($metainfo['t']) == 'object'){
										$word = get_object_vars($metainfo['t']);
										$word = $word[0];
									}
									else{
										$word = $metainfo['t'];
									}
									
									$tempdataStructure['body'][$count['maindata']]['content'][$count['listitem']]['data'][$count['words']]['word'] = $word;
									$count['words']++;
								}
							}
						}
					break;
				}
		}
		return $tempdataStructure;

	}
	
	/*
		publish HTML
	*/
	
	public function publishHTML($dataStructure){
		echo "<pre>";
			print_r($dataStructure);
		echo "</pre>";
		$str = "<html><style>p{ word-wrap: break-word; }</style><body style='width: 595.28px; margin: auto;'><div style='border: 1px solid gray;'>";
		
		foreach($dataStructure['body'] as $data){
			if($data['metainfo']['type'] == 'list'){
				$str = $str."<ul>";
				foreach($data['content'] as $content){
					$str = $str."<li style='color: #".$content['data'][0]['color'].";font-size: ".$content['data'][0]['fontsize']."px;text-align:".$content['meta']['align']."'>".$content['data'][0]['word']."</span></li>";
				}
				$str = $str."</ul>";
			}
			else if($data['metainfo']['type'] == 'paragraph'){
				$str = $str."<p style='text-align: ".$data['metainfo']['align'].";'>";
				foreach($data['content'] as $content){
					//print_r($content);
					$str = $str.'<span style="font-size: '.$content['fontsize'].'px;color: #'.$content['color'].';">'.$content['word'].'</span>';
				}
				$str = $str."</p>";
			}
		}
		
		$str = $str."</div></body></html>";
		
		return $str;
	}

	/*
		extract font relationships
	*/
	public function generateFontTable($fonttarget){
		$fpxml = simplexml_load_file(($this->extractToDir).$this->mainDocumentDir.'/'.$fonttarget);
		$fpxml->registerXPathNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
		return $fpxml;
	}
	
	/*
		0 : paragraph properties does not exist
		paragraph properties exist and 
		1 : list
	*/	
	public function identifyParaType($pPr,$r){
		if(gettype($pPr) == 'NULL'){
			if(gettype($r) == 'NULL'){
				return 0;
			}
			else{
				return 1;
			}
		}
		else{
			$pstyle = $pPr->pStyle;

			if(sizeof($pstyle) == 0){
				return 1;
			}
			else{
				$pstyle = get_object_vars($pstyle);
				$pstyle = $pstyle['@attributes']['val'];
				switch($pstyle){
					case 'ListParagraph': 
						return 2;
						break;
				}	
			}
		}
	}
	
}

?>